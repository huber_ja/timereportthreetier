package com.jhuber.timereportthreetier;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Janina Huber
 */
@Provider
public class TimereportExceptionHandler implements ExceptionMapper<TimereportException>{

    @Override
    public Response toResponse(TimereportException exception) {
        return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).type("text/plain").build(); //
    }
    
   
    
}
