package com.jhuber.timereportthreetier.employee;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.EmployeeService;
import java.util.ArrayList;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Janina
 */
@Stateless
@Path("employee")
public class EmployeeResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeResource.class);

    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

    @EJB
    public EmployeeService employeeService;

    @EJB
    public CompanyService companyService;

   
    @GET
    public String employees() {
        List<Employee> employees = new ArrayList<>();
        try {
            employees = employeeService.findAll();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return GSON_BUILDER.toJson(employees);
    }

    @POST
    @Path("/new")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED + "; charset=ISO-8859-1;")
    public void addEmployee(
            @FormParam("firstname") String firstname,
            @FormParam("lastname") String lastname,
            @FormParam("employeenumber") String employeenumber,
            @FormParam("emplpassw") String passwort,
            @FormParam("emplpasswrep") String passwortrepeat,
            @FormParam("company") Long companyid,
            @FormParam("loginname") String loginname,
            @Context HttpServletResponse servletResponse) throws TimereportException {

        
        Timereportuser checkTimereportuser = employeeService.findTimereportUserByLoginname(loginname);
        if (checkTimereportuser != null) {
            throw new TimereportException("User " + loginname + " existiert. Bitte anderen loginnamen wählen.");
        }
       
        Employee checkEmployeeNumber = employeeService.findByEmployeeNumber(employeenumber);
        if(checkEmployeeNumber!=null){
            throw new TimereportException("Employeenumber " + employeenumber + " existiert. Bitte andere Nummer wählen.");
        }
       
        
        Company company = companyService.findById(companyid);
        if (company != null) {
            if(passwort.equals(passwortrepeat)){
                createEmployee(company, employeenumber, firstname, lastname, passwort, loginname);
            }else{
                throw new TimereportException("Felder passwort und passwortrepeat sind nicht identisch. Geben Sie Ihr Passwort noch mal ein.");
            }
            servletResponse.setHeader("Location", "timereport.html");
            servletResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);

        } else {
            throw new TimereportException("Feld Company ungültig.");
        }

    }

    private void createEmployee(Company company, String employeenumber, String firstname, String lastname, String passwort, String loginname) {
        
        
        Employee newEmployee = new Employee();
        newEmployee.setCompany(company);
        newEmployee.setEmployeenumber(employeenumber);
        newEmployee.setFirstname(firstname);
        newEmployee.setLastname(lastname);
        
        String hash = Hashing.sha256().hashString(passwort, Charsets.UTF_8).toString();
        
        Timereportuser timereportuser = new Timereportuser();
        timereportuser.setUsername(loginname);
        timereportuser.setPassword(hash);
        
        employeeService.createTimereportuser(timereportuser);
        
        newEmployee.setTimereportuser(timereportuser);
        
        employeeService.create(newEmployee);
    }

    @DELETE
    @Path("/delete/{employeeNumber}")
    public void deleteEmployee(
            @PathParam("employeeNumber") String employeeNumber,
            @Context HttpServletResponse servletResponse) {

        Employee employee = employeeService.findByEmployeeNumber(employeeNumber);

        if (employee != null) {

            employeeService.delete(employee);

        }

        servletResponse.setStatus(HttpServletResponse.SC_OK);

    } 
    
}
