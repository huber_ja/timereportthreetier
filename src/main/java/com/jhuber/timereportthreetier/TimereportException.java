package com.jhuber.timereportthreetier;

import java.io.Serializable;

/**
 *
 * @author Janina Huber
 */
public class TimereportException extends Exception implements Serializable{
    private static final long serialVersionUID = 1L;
    public TimereportException() {
        super();
    }
    public TimereportException(String msg)   {
        super(msg);
    }
    public TimereportException(String msg, Exception e)  {
        super(msg, e);
    }
    
}
