package com.jhuber.timereportthreetier.company;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Federalstate;

import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import java.util.ArrayList;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina
 */
@Stateless
@Path("company")
public class CompanyResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyResource.class);

    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").serializeNulls().create();

    private static enum CompanyAttributes {

        companyname, address, federalstate, middaybreakfrom, middaybreakduration
    };

    private static final String COMPANY_KANN_NICHT_GEAENDERT_WERDEN = "Company kann nicht geändert werden.";

    @EJB
    public CompanyService companyService;

    @EJB
    public FederalstateService federalstateService;
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String allCompanies() {
        List<Company> companies = new ArrayList<>();
        try {
            companies = companyService.findAll();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return GSON_BUILDER.toJson(companies);
    }

    @POST
    @Path("/new")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED + "; charset=ISO-8859-1;")
    public void addCompany(
            @FormParam("name") String name,
            @FormParam("address") String address,
            @FormParam("federalstate") String federalstatecode,
            @FormParam("middaybreakfrom") int middaybreakfrom,
            @FormParam("middaybreakduration") int middaybreakduration,
            @Context HttpServletResponse servletResponse) throws TimereportException {

        createNewCompany(federalstatecode, name, address, middaybreakduration, middaybreakfrom);

        servletResponse.setHeader("Location", "companies.html");
        servletResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
    }

    private void createNewCompany(String federalstatecode, String name, String address, int middaybreakduration, int middaybreakfrom) throws TimereportException {
        Federalstate fedState = federalstateService.findByCode(federalstatecode);

        if (fedState != null) {
            Company newCompany = new Company();
            newCompany.setName(name);
            newCompany.setAddress(address);
            newCompany.setFederalstate(fedState);
            newCompany.setMiddaybreakduration(middaybreakduration);
            newCompany.setMiddaybreakfrom(middaybreakfrom);

            try {
                companyService.create(newCompany);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }

        } else {
            throw new TimereportException("Bundesland ist ungültig");
        }
    }

    @POST
    @Path("/update")
    public void updateCompany(
            @FormParam("companyId") Long companyId,
            @FormParam("companyProp") String companyProp,
            @FormParam("value") String value,
            @Context HttpServletResponse servletResponse) throws TimereportException {

        Company company = companyService.findById(companyId);

        if (company != null) {
            switch (CompanyAttributes.valueOf(companyProp)) {
                case companyname:
                    company.setName(value);
                    break;
                case federalstate:
                    updateCompanyFederalstate(value, company);
                    break;
                case address:
                    company.setAddress(value);
                    break;
                case middaybreakduration:
                    updateCompanyMiddaybreakduration(company, value);
                    break;
                case middaybreakfrom:
                    updateCompanyMiddaybreakfrom(company, value);
                    break;
                default:
                    break;
            }

            companyService.update(company);
        }
        servletResponse.setStatus(HttpServletResponse.SC_OK);

    }

    private void updateCompanyMiddaybreakfrom(Company company, String value) throws TimereportException {
        try {
            company.setMiddaybreakfrom(Integer.parseInt(value));
        } catch (NumberFormatException ex) {
            LOGGER.debug(COMPANY_KANN_NICHT_GEAENDERT_WERDEN, ex);
            throw new TimereportException(COMPANY_KANN_NICHT_GEAENDERT_WERDEN);
        }
    }

    private void updateCompanyMiddaybreakduration(Company company, String value) throws TimereportException {
        try {
            company.setMiddaybreakduration(Integer.parseInt(value));
        } catch (NumberFormatException ex) {
            LOGGER.debug(COMPANY_KANN_NICHT_GEAENDERT_WERDEN, ex);
            throw new TimereportException(COMPANY_KANN_NICHT_GEAENDERT_WERDEN);
        }
    }

    private void updateCompanyFederalstate(String value, Company company) throws TimereportException {
        Federalstate fedState = federalstateService.findByCode(value);
        if (fedState != null) {
            company.setFederalstate(fedState);
        } else {
            throw new TimereportException(COMPANY_KANN_NICHT_GEAENDERT_WERDEN);
        }
    }

    @DELETE
    @Path("/delete/{companyId}")
    public void deleteCompany(
            @PathParam("companyId") Long companyId,
            @Context HttpServletResponse servletResponse) {

        Company company = companyService.findById(companyId);

        if (company != null) {
            companyService.delete(company);
        }
        servletResponse.setStatus(HttpServletResponse.SC_OK);

    }
}
