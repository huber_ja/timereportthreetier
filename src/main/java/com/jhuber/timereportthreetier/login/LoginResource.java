package com.jhuber.timereportthreetier.login;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.service.EmployeeService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina Huber
 */
@Stateless
@Path("auth")
public class LoginResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginResource.class);

    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

    @EJB
    public EmployeeService employeeService;
    
    @GET
    @Path("/logout")
    public void logout(@Context HttpServletRequest request,
            @Context HttpServletResponse servletResponse,
            @Context SecurityContext sec) {

        if (sec!=null && sec.getUserPrincipal() != null) {
            LOGGER.info("logout user:" + sec.getUserPrincipal().getName());
        } else {
            LOGGER.info("NO LOGIN USER!!!");
        }
        try {
            request.logout();
            servletResponse.sendRedirect("../../");
        } catch (ServletException | IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

    }

    @GET
    @Path("/empl")
    public String findEmployee(@Context HttpServletResponse servletResponse,
            @Context SecurityContext sec) {

        StringBuilder employeeName = new StringBuilder();
        try {
            if (sec!=null && sec.getUserPrincipal() != null) {
                Employee empl = employeeService.findByLoginName(sec.getUserPrincipal().getName());
                if (empl == null) {
                    LOGGER.error("Kein Employee fuer " + sec.getUserPrincipal());
                    servletResponse.sendRedirect("../../");
                }else{
                    employeeName.append(empl.getFirstname()).append(" ").append(empl.getLastname());
                }

            } else {
                servletResponse.sendRedirect("../../");
            }
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return GSON_BUILDER.toJson(employeeName.toString());

    }
}
