package com.jhuber.timereportthreetier;

import com.jhuber.timereportthreetier.company.CompanyResource;
import com.jhuber.timereportthreetier.employee.EmployeeResource;
import com.jhuber.timereportthreetier.federalstate.FederalstateResource;
import com.jhuber.timereportthreetier.login.LoginResource;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayResource;
import com.jhuber.timereportthreetier.timereport.TimereportResource;
import com.jhuber.timereportthreetier.workingday.WorkingdayResource;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 *
 * @author Janina
 */
@ApplicationPath("rest")
public class JAXRSConfiguration extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(TimereportResource.class);
        resources.add(MultiPartFeature.class);
        resources.add(CompanyResource.class);
        resources.add(EmployeeResource.class);
        resources.add(FederalstateResource.class);
        resources.add(PublicholidayResource.class);
        resources.add(LoginResource.class);
        resources.add(TimereportExceptionHandler.class);
        resources.add(WorkingdayResource.class);
        return resources;
    }

}
