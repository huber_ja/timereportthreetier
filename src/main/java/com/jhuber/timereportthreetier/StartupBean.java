package com.jhuber.timereportthreetier;

import com.google.gson.Gson;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Publicholiday;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayCallImportURL;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayMapper;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayWebservice;
import com.jhuber.timereportthreetier.service.FederalstateService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina Huber
 */
@Singleton
@Startup
public class StartupBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupBean.class);
    
    private EntityManager em;

    private PublicholidayService publicholidayService;

    private FederalstateService federalstateService;

    @PostConstruct
    void startup() {
        LOGGER.info("Service Started. Update publicholidays");

        this.em = Persistence.createEntityManagerFactory("timereportThreetierUnit").createEntityManager();

        this.publicholidayService = new PublicholidayService();
        this.publicholidayService.setEm(em);

        this.federalstateService = new FederalstateService();
        this.federalstateService.setEm(em);

        updatePublicholidays();

        this.em.clear();
        this.em.close();
    }

    @PreDestroy
    void shutdown() {
       LOGGER.info("Service shutdown.");
    }

    private void updatePublicholidays() {

        List<Federalstate> federalstates = federalstateService.findAll();

        if(federalstates.size()!=16){
            
            LOGGER.error("Number of Federalstates is incorrect, "+federalstates.size()+" found. Check and update the database!!!");
        }
        
        int insertedHolidays = 0;
        
        for (Federalstate federalstate : federalstates) {

            String jsonString = PublicholidayCallImportURL.getJsonStringByFederalstate(federalstate.getCode());

            Gson gson = new Gson();
            PublicholidayWebservice[] publicholidayFromWebservices = gson.fromJson(jsonString, PublicholidayWebservice[].class);

            for (PublicholidayWebservice publHoliFromWebservice : publicholidayFromWebservices) {
                Publicholiday importHoliday = PublicholidayMapper.getEntityFromWebserviceObj(publHoliFromWebservice, federalstate);

                if (importHoliday!=null && publicholidayService.findPublicholidayObj(importHoliday) == null) {
                    publicholidayService.create(importHoliday);
                    LOGGER.info("Insert " + importHoliday);
                    insertedHolidays++;
                }

            }
            
            LOGGER.info(insertedHolidays+" Publicholiday imported.");
            
        }
    }

}
