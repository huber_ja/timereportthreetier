package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author Janina
 */
@Entity
public class Timereport implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Expose
    @Column(nullable = false)
    private int reportYear;
    
    @Expose
    @Column(nullable = false)
    private int reportMonth;
       
    @Expose
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Expose
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;
    
    @Expose
    @ManyToOne
    private Employee employee;
    
    @OneToMany(mappedBy = "timereport")
    private List<Workingday> workingDays;

    public Timereport() {
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getReportYear() {
        return reportYear;
    }

    public void setReportYear(int reportYear) {
        this.reportYear = reportYear;
    }

    public int getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(int reportMonth) {
        this.reportMonth = reportMonth;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Workingday> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Workingday> workingDays) {
        this.workingDays = workingDays;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
     
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Timereport)) {
            return false;
        }
        Timereport other = (Timereport) object;
        
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jhuber.timereportthreetier.data.Timereport[ id=" + id + " ]";
    }
    
}
