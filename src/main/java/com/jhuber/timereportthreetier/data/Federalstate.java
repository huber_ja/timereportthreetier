package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author Janina
 */
@Entity
public class Federalstate implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Expose
    @Column(nullable = false, length = 50)
    private String name;
    
    @Expose
    @Column(nullable = false, length = 3)
    private String code;
    
    @OneToMany(mappedBy = "federalstate", fetch = FetchType.LAZY)
    private List<Company> companies;
    
    @OneToMany(mappedBy = "federalstate", fetch = FetchType.LAZY)
    private List<Publicholiday> publicholidays;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;

    public Federalstate() {
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }
    
    public List<Publicholiday> getPublicholidays() {
        return publicholidays;
    }

    public void setPublicholidays(List<Publicholiday> publicholidays) {
        this.publicholidays = publicholidays;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
       
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Federalstate)) {
            return false;
        }
        Federalstate other = (Federalstate) object;
        
        if ((this.id == null && other.id!= null) 
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Federalstate{" + "id=" + id + ", name=" + name + ", code=" + code + ", created=" + created + ", lastModified=" + lastModified + '}';
    }

    
    
}
