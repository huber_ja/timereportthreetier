package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author Janina
 */
@Entity
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Expose
    @Column(nullable = false, length = 20)
    private String firstname;
    
    @Expose
    @Column(nullable = false, length = 50)
    private String lastname;
    
    @Expose
    @Column(nullable = false, length = 30)
    private String employeenumber;
    
    @Expose
    @ManyToOne
    private Company company;
    
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<Timereport> timereports;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;

    private Boolean disabled;
    
   
    @OneToOne(optional = false)
    private Timereportuser timereportuser;
    
    
    public Employee() {
    }

    public Employee(String firstname, String lastname, String employeenumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.employeenumber = employeenumber;
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmployeenumber() {
        return employeenumber;
    }

    public void setEmployeenumber(String employeenumber) {
        this.employeenumber = employeenumber;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Timereport> getTimereports() {
        return timereports;
    }

    public void setTimereports(List<Timereport> timereports) {
        this.timereports = timereports;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Timereportuser getTimereportuser() {
        return timereportuser;
    }

    public void setTimereportuser(Timereportuser timereportuser) {
        this.timereportuser = timereportuser;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        
        Employee other = (Employee) object;
         
        if ((this.id == null && other.id != null) 
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jhuber.timereportthreetier.data.Employee[ id=" + id + " ]";
    }
    
    
    
}
