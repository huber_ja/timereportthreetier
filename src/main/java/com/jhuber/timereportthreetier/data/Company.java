package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author Janina Huber
 */
@Entity
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Expose
    @Column(nullable=false,length=50)
    private String name;
    
    @Expose
    @Column(length=100)
    private String address;
    
    @Expose
    @ManyToOne(fetch = FetchType.EAGER)
    private Federalstate federalstate;
    
    @Expose
    @Column(nullable=false)
    private int middaybreakfrom;
    
    @Expose
    @Column(nullable=false)
    int middaybreakduration;
    
    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    List<Employee> employees;
   
    private Boolean disabled;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;

    
    public Company() {
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Federalstate getFederalstate() {
        return federalstate;
    }

    public void setFederalstate(Federalstate federalstate) {
        this.federalstate = federalstate;
    }

    public int getMiddaybreakfrom() {
        return middaybreakfrom;
    }

    public void setMiddaybreakfrom(int middaybreakfrom) {
        this.middaybreakfrom = middaybreakfrom;
    }

    public int getMiddaybreakduration() {
        return middaybreakduration;
    }

    public void setMiddaybreakduration(int middaybreakduration) {
        this.middaybreakduration = middaybreakduration;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
       
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Company)) {
            return false;
        }
        Company other = (Company) object;
       
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jhuber.timereportthreetierjpa.data.Company[ id=" + id + " ]";
    }
    
}
