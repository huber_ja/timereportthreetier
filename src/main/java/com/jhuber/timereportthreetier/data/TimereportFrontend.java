package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Janina
 */
public class TimereportFrontend implements Serializable {
    private static final long serialVersionUID = 1L;

    @Expose
    private String reportPeriod;
    
    @Expose
    private double workingHours;
    
    @Expose
    private double overtimeHours;
    
    @Expose
    private double minusHours;
    
    @Expose
    private double vacationdays;
    
    @Expose
    private List<WorkingdayFrontend> workingDays;

    public TimereportFrontend() {
    }

    public String getReportPeriod() {
        return reportPeriod;
    }

    public void setReportPeriod(String reportPeriod) {
        this.reportPeriod = reportPeriod;
    }

    public double getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(double workingHours) {
        this.workingHours = workingHours;
    }

    public double getOvertimeHours() {
        return overtimeHours;
    }

    public void setOvertimeHours(double overtimeHours) {
        this.overtimeHours = overtimeHours;
    }

    public double getVacationdays() {
        return vacationdays;
    }

    public void setVacationdays(double vacationdays) {
        this.vacationdays = vacationdays;
    }

    public List<WorkingdayFrontend> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<WorkingdayFrontend> workingDays) {
        this.workingDays = workingDays;
    }

    public double getMinusHours() {
        return minusHours;
    }

    public void setMinusHours(double minusHours) {
        this.minusHours = minusHours;
    }

    @Override
    public String toString() {
        return "TimereportFrontend{" + "reportPeriod=" + reportPeriod + ", workingHours=" + workingHours + ", overtimeHours=" + overtimeHours + ", minusHours=" + minusHours + ", vacationdays=" + vacationdays + ", workingDays=" + workingDays + '}';
    }
}
