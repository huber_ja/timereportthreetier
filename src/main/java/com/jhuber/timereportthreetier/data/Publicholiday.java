package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Janina Huber
 */
@Entity
public class Publicholiday implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Expose
    @NotNull
    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date holidayDate;

    @Expose
    @Column(length=500)
    private String holidayDescription;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;

    @Expose
    @ManyToOne
    private Federalstate federalstate;

    public Publicholiday() {
    }

    public Publicholiday(Date holidayDate) {
        this.holidayDate = holidayDate;
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }

    public Federalstate getFederalstate() {
        return federalstate;
    }

    public void setFederalstate(Federalstate federalstate) {
        this.federalstate = federalstate;
    }

    public Date getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(Date holidayDate) {
        this.holidayDate = holidayDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getHolidayDescription() {
        return holidayDescription;
    }

    public void setHolidayDescription(String holidayDescription) {
        this.holidayDescription = holidayDescription;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
    
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicholiday)) {
            return false;
        }
        Publicholiday other = (Publicholiday) object;

        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Publicholiday{" + "id=" + id + ", holidayDate=" + holidayDate + ", holidayDescription=" + holidayDescription + ", created=" + created + ", lastModified=" + lastModified + ", federalstate=" + federalstate + '}';
    }

    

}
