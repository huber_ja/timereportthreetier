package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Janina
 */

public class WorkingdayFrontend implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    private Long id;
    
    @Expose
    private Date workindDayDate;
    
    @Expose
    private String workindDayStart;
    
    @Expose
    private String workindDayEnd;
    
    @Expose
    private boolean publicholiday;
    
    @Expose
    private boolean vacationday;
   
    @Expose
    private int calendarWeek;
   
    @Expose
    private String workinghours;
    
    public WorkingdayFrontend() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getWorkindDayDate() {
        return workindDayDate;
    }

    public void setWorkindDayDate(Date workindDayDate) {
        this.workindDayDate = workindDayDate;
    }

    public boolean isPublicholiday() {
        return publicholiday;
    }

    public void setPublicholiday(boolean publicholiday) {
        this.publicholiday = publicholiday;
    }

    public boolean isVacationday() {
        return vacationday;
    }

    public void setVacationday(boolean vacationday) {
        this.vacationday = vacationday;
    }

    public int getCalendarWeek() {
        return calendarWeek;
    }

    public void setCalendarWeek(int calendarWeek) {
        this.calendarWeek = calendarWeek;
    }

    public String getWorkindDayStart() {
        return workindDayStart;
    }

    public void setWorkindDayStart(String workindDayStart) {
        this.workindDayStart = workindDayStart;
    }

    public String getWorkindDayEnd() {
        return workindDayEnd;
    }

    public void setWorkindDayEnd(String workindDayEnd) {
        this.workindDayEnd = workindDayEnd;
    }

    public String getWorkinghours() {
        return workinghours;
    }

    public void setWorkinghours(String workinghours) {
        this.workinghours = workinghours;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.workindDayDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkingdayFrontend other = (WorkingdayFrontend) obj;
        if (!Objects.equals(this.workindDayDate, other.workindDayDate)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "WorkingdayFrontend{" + "id=" + id + ", workindDayDate=" + workindDayDate + ", workindDayStart=" + workindDayStart + ", workindDayEnd=" + workindDayEnd + ", publicholiday=" + publicholiday + ", vacationday=" + vacationday + ", calendarWeek=" + calendarWeek + ", workinghours=" + workinghours + '}';
    }

}
