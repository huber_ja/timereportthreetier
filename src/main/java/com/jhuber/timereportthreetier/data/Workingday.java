package com.jhuber.timereportthreetier.data;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author Janina
 */
@Entity
public class Workingday implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Expose
    @Column(nullable = false)
    private Date workindDayDate;
    
    @Expose
    @Column(nullable = false)
    private double workindDayStart;
    
    @Expose
    @Column(nullable = false)
    private double workindDayEnd;
    
    @Expose
    private boolean publicholiday;
    
    @Expose
    private boolean vacationday;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModified;
    
    @ManyToOne
    private Timereport timereport;

    public Workingday() {
    }
    
    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastModified = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getWorkindDayDate() {
        return workindDayDate;
    }

    public void setWorkindDayDate(Date workindDayDate) {
        this.workindDayDate = workindDayDate;
    }

    public double getWorkindDayStart() {
        return workindDayStart;
    }

    public void setWorkindDayStart(double workindDayStart) {
        this.workindDayStart = workindDayStart;
    }

    public double getWorkindDayEnd() {
        return workindDayEnd;
    }

    public void setWorkindDayEnd(double workindDayEnd) {
        this.workindDayEnd = workindDayEnd;
    }

    public boolean isPublicholiday() {
        return publicholiday;
    }

    public void setPublicholiday(boolean publicholiday) {
        this.publicholiday = publicholiday;
    }

    public boolean isVacationday() {
        return vacationday;
    }

    public void setVacationday(boolean vacationday) {
        this.vacationday = vacationday;
    }

    public Timereport getTimereport() {
        return timereport;
    }

    public void setTimereport(Timereport timereport) {
        this.timereport = timereport;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Workingday)) {
            return false;
        }
        Workingday other = (Workingday) object;
        
        if ((this.id == null && other.id!= null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Workingday{" + "id=" + id + ", workindDayDate=" + workindDayDate + ", workindDayStart=" + workindDayStart + ", workindDayEnd=" + workindDayEnd + ", publicholiday=" + publicholiday + ", vacationday=" + vacationday + ", timereport=" + timereport + '}';
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
    
    
    
}
