package com.jhuber.timereportthreetier.publicholiday;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.jhuber.timereportthreetier.data.Publicholiday;
import com.jhuber.timereportthreetier.service.FederalstateService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina
 */
@Stateless
@Path("publicholiday")
public class PublicholidayResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(PublicholidayResource.class);
    
    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").serializeNulls().create(); 
    
    @EJB
    public PublicholidayService publicholidayService;

    @EJB
    public FederalstateService federalstateService;
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String allPublicholidays() {
        List<Publicholiday> publicholidays = new ArrayList<>();
        try {
            publicholidays = publicholidayService.findAll();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
       
        return GSON_BUILDER.toJson(publicholidays);
    }

    @GET
    @Path("{federalstatecode}")
    @Produces(MediaType.TEXT_PLAIN)
    public String findPublicholidayByFederalStateCode(@PathParam("federalstatecode") String federalstatecode){
        List<Publicholiday> publicholidays = new ArrayList<>();
        try {
            publicholidays = publicholidayService.findByFederalstateCode(federalstatecode);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        
        return GSON_BUILDER.toJson(publicholidays);
    }
    
    @GET
    @Path("{federalstate}/{date}")
    @Produces(MediaType.TEXT_PLAIN)
    public String findPublicholidayByFederalstateAndDate(@PathParam("federalstate") String federalstatecode, @PathParam("date") String date){
        
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        List<Publicholiday> publicholidays = new ArrayList<>();
        try {
            publicholidays = publicholidayService.findByFederalstateCodeAndDate(federalstatecode, formatter.parse(date));

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
          
        return GSON_BUILDER.toJson(publicholidays);
    
    }

}
