package com.jhuber.timereportthreetier.publicholiday;

import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Publicholiday;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina Huber
 */
public class PublicholidayMapper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicholidayMapper.class);
    
    private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    private PublicholidayMapper() {
    }
    
    public static Publicholiday getEntityFromWebserviceObj(PublicholidayWebservice publHoliFromWebservice, Federalstate federalstate) {
        
        try {
            Publicholiday holidayToImport = new Publicholiday();
            holidayToImport.setFederalstate(federalstate);
            holidayToImport.setHolidayDescription(publHoliFromWebservice.getTitle());
            holidayToImport.setHolidayDate(FORMATTER.parse(publHoliFromWebservice.getDatum()));
            
            return holidayToImport;
            
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage());
        }
        
        return null;
    }
    
}
