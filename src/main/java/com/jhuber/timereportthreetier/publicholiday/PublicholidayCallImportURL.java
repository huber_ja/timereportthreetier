package com.jhuber.timereportthreetier.publicholiday;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina Huber
 */
public class PublicholidayCallImportURL {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicholidayCallImportURL.class);
    
    private static final String JARMEDIA_URL ="http://feiertage.jarmedia.de/api/?";

    private PublicholidayCallImportURL() {
    }
    
    
    
    public static String getJsonStringByFederalstate(String federalStateCode) {
        
        Calendar dateNow = Calendar.getInstance();
        int yearNow = dateNow.get(Calendar.YEAR);
        StringBuilder callURL = new StringBuilder();
        callURL.append(JARMEDIA_URL);
        callURL.append("jahr=");
        callURL.append(yearNow);
        callURL.append("&nur_land=");
        callURL.append(federalStateCode);
        
        StringBuilder sb = callImportURL(callURL.toString());

        return modifyJsonString(sb.toString());
    }

    private static StringBuilder callImportURL(String callURL) {
        
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        try {
            URL url = new URL(callURL);
            urlConn = url.openConnection();
            
            if (urlConn != null) {
                urlConn.setReadTimeout(60 * 1000);
            }
            
            if (urlConn != null && urlConn.getInputStream() != null) {
                
               InputStreamReader in = readInputstream(urlConn, sb);
               in.close();
            }
            
        } catch (Exception e) {
            LOGGER.error("Exception while calling URL:" + callURL, e);
        }
        return sb;
    }

    private static InputStreamReader readInputstream(URLConnection urlConn, StringBuilder sb) throws IOException {
        InputStreamReader in = new InputStreamReader(urlConn.getInputStream(),
                Charset.defaultCharset());
        BufferedReader bufferedReader = new BufferedReader(in);
        if (bufferedReader != null) {
            int cp;
            while ((cp = bufferedReader.read()) != -1) {
                sb.append((char) cp);
            }
            bufferedReader.close();
        }
        return in;
    }
    
    
    private static String modifyJsonString(String jsonStringFromWebService) {
        String jsonString = jsonStringFromWebService.substring(1,jsonStringFromWebService.length());
        jsonString = jsonString.substring(0, jsonString.length()-1);
        jsonString = jsonString.replaceAll(Pattern.quote(":{"), ",");
        jsonString = jsonString.replaceAll("},", "},{\"title\"=");
        StringBuilder jsonStringForJava = new StringBuilder();
        jsonStringForJava.append("[");
        jsonStringForJava.append("{\"title\"=");
        jsonStringForJava.append(jsonString);
        jsonStringForJava.append("]");
        return jsonStringForJava.toString();
    }
    
}
