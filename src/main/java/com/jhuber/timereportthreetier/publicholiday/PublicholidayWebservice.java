package com.jhuber.timereportthreetier.publicholiday;

/**
 *
 * @author Janina Huber
 */
public class PublicholidayWebservice {

    private String title;
    private String datum;
    private String hinweis;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    

    public String getHinweis() {
        return hinweis;
    }

    public void setHinweis(String hinweis) {
        this.hinweis = hinweis;
    }

    @Override
    public String toString() {
        return "PublicholidayFromWebservice{" + "title=" + title + ", date=" + datum + ", hinweis=" + hinweis + '}';
    }

    
    
}
