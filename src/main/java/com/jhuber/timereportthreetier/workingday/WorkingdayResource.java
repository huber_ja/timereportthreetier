package com.jhuber.timereportthreetier.workingday;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Publicholiday;
import com.jhuber.timereportthreetier.data.TimereportFrontend;
import com.jhuber.timereportthreetier.data.Workingday;
import com.jhuber.timereportthreetier.data.WorkingdayFrontend;
import com.jhuber.timereportthreetier.service.EmployeeService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import com.jhuber.timereportthreetier.service.TimereportService;
import com.jhuber.timereportthreetier.service.WorkingdayService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Janina
 */
@Stateless
@Path("workingday")
public class WorkingdayResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkingdayResource.class);

    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

    private static final Double WORKING_HOURS_PER_DAY_NORM = 8.0;

    @EJB
    public TimereportService timereportService;

    @EJB
    public EmployeeService employeeService;

    @EJB
    public WorkingdayService workingdayService;

    @EJB
    public PublicholidayService publicholidayService;
    
    @GET
    @Path("/list")
    public String workingdays(@Context SecurityContext sec) throws TimereportException {

        String loginname = sec.getUserPrincipal().getName();

        Employee employee = employeeService.findByLoginName(loginname);
        if (employee == null) {
            LOGGER.warn("Kein Employee für login " + loginname + " gefunden");
            throw new TimereportException("Bitte melden Sie sich an.");
        }

        Calendar dateNow = getCalendarNow();
        dateNow.set(Calendar.DAY_OF_MONTH, 1);

        List<WorkingdayFrontend> emptyWorkingdayFrontendList = new ArrayList<>();
        List<Workingday> workingdays = filterByMonth(String.valueOf(dateNow.get(Calendar.MONTH)), dateNow.get(Calendar.YEAR), employee, emptyWorkingdayFrontendList);

        TimereportFrontend timereportFrontend = parseWorkingdayEntitiesToFrontend(workingdays, employee.getCompany(), emptyWorkingdayFrontendList);
        timereportFrontend.setReportPeriod(TimereportMonthsNames.values()[dateNow.get(Calendar.MONTH)] + " " + dateNow.get(Calendar.YEAR));
        return GSON_BUILDER.toJson(timereportFrontend);
    }

    @POST
    @Path("/filter")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED + "; charset=ISO-8859-1;")
    public String workingdaysFilter(
            @FormParam("calendarweek") String calendarWeek,
            @FormParam("month") String month,
            @FormParam("year") String year,
            @Context SecurityContext sec) throws TimereportException {

        String loginname = sec.getUserPrincipal().getName();

        Employee employee = employeeService.findByLoginName(loginname);
        if (employee == null) {
            LOGGER.warn("Kein Employee für login " + loginname + " gefunden");
            throw new TimereportException("Bitte melden Sie sich an.");
        }
 
        if ((calendarWeek == null || calendarWeek.isEmpty())
                && (month == null || month.isEmpty())
                && (year == null || year.isEmpty())) {
            throw new TimereportException("Bitte Woche, Monat oder Jahr eingeben.");

        }

        TimereportFrontend timereportFrontend = fillTimereportFrontend(calendarWeek, employee, year, month);

        return GSON_BUILDER.toJson(timereportFrontend);
    }

    private TimereportFrontend fillTimereportFrontend(String calendarWeek, Employee employee, String year, String month) throws TimereportException {
        
        int yearAsInt = setYear(year);
        
        List<Workingday> workingdays = new ArrayList<>();
        StringBuilder reportPeriod = new StringBuilder();
        List<WorkingdayFrontend> emptyWorkingdayFrontendList = new ArrayList<>();
        if (calendarWeek != null && calendarWeek.length() > 0) {
            workingdays = filterByCalenderWeek(calendarWeek, yearAsInt, employee, emptyWorkingdayFrontendList);
            reportPeriod.append("Calenderweek ").append(calendarWeek).append(" ").append(year);

        } else if (month != null && month.length() > 0) {
            workingdays = filterByMonth(month, yearAsInt, employee, emptyWorkingdayFrontendList);
            reportPeriod.append(TimereportMonthsNames.values()[Integer.valueOf(month)]).append(" ").append(year);
        } else {
            workingdays = filterByYear(yearAsInt, employee, emptyWorkingdayFrontendList);
            reportPeriod.append(year);
        }
        TimereportFrontend timereportFrontend = parseWorkingdayEntitiesToFrontend(workingdays, employee.getCompany(), emptyWorkingdayFrontendList);
        timereportFrontend.setReportPeriod(reportPeriod.toString());
        return timereportFrontend;
    }

    private int setYear(String year) throws TimereportException {
        int yearAsInt = Calendar.getInstance().get(Calendar.YEAR);
        if (year != null && year.length() > 0) {
            try {
                yearAsInt = Integer.parseInt(year);
            } catch (NumberFormatException ex) {
                LOGGER.debug("Bitte Jahr als Zahl eingeben.", ex);
                throw new TimereportException("Bitte Jahr als Zahl eingeben.");
            }
        }
        return yearAsInt;
    }

    
    private List<Workingday> filterByCalenderWeek(String calendarWeek, int year, Employee employee, List<WorkingdayFrontend> emptyWorkingdayFrontendList) throws TimereportException {
        List<Workingday> workingdays = new ArrayList<>();
        try {
            Calendar dateNow = getCalendarNow();

            int calendarWeekAsInt = Integer.parseInt(calendarWeek);
            dateNow.set(Calendar.WEEK_OF_YEAR, calendarWeekAsInt);
            dateNow.set(Calendar.YEAR, year);

            Calendar dateMin = (Calendar) dateNow.clone();
            resetDayTime(dateMin);

            dateMin.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            Date minDate = dateMin.getTime();

            Calendar dateMax = (Calendar) dateMin.clone();
            resetDayTime(dateMax);
            dateMax.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            Date maxDate = dateMax.getTime();

            workingdays = workingdayService.findByEmployeeAndBetweenDate(employee, minDate, maxDate);

            createEmptyWorkingdayFrontendList(dateMax.getActualMaximum(Calendar.DAY_OF_WEEK), dateMin, emptyWorkingdayFrontendList, employee.getCompany().getFederalstate().getCode());

        } catch (NumberFormatException ex) {
            LOGGER.debug("Bitte Kalenderwoche als Zahl eingeben.", ex);
            throw new TimereportException("Bitte Kalenderwoche als Zahl eingeben.");
        }
        return workingdays;
    }

    private void resetDayTime(Calendar dateMin) {
        dateMin.set(Calendar.HOUR_OF_DAY, 0);
        dateMin.set(Calendar.MINUTE, 0);
        dateMin.set(Calendar.SECOND, 0);
        dateMin.set(Calendar.MILLISECOND, 0);
    }

    private void createEmptyWorkingdayFrontendList(int daysCount, Calendar dateMin, List<WorkingdayFrontend> emptyWorkingdayFrontendList, String federalstatecode) {

        for (int dateDay = 0; dateDay < daysCount; dateDay++) {
            WorkingdayFrontend emptyWorkingdayFrontend = new WorkingdayFrontend();
            Calendar nextDate = (Calendar) dateMin.clone();
            nextDate.add(Calendar.DAY_OF_YEAR, dateDay);
            emptyWorkingdayFrontend.setWorkindDayDate(nextDate.getTime());
            emptyWorkingdayFrontend.setCalendarWeek(nextDate.get(Calendar.WEEK_OF_YEAR));

            emptyWorkingdayFrontend.setWorkindDayEnd("");
            emptyWorkingdayFrontend.setWorkindDayStart("");
            emptyWorkingdayFrontend.setWorkinghours("");
            emptyWorkingdayFrontend.setVacationday(false);
            emptyWorkingdayFrontend.setPublicholiday(false);
            List<Publicholiday> publicholidays = publicholidayService.findByFederalstateCodeAndDate(federalstatecode, nextDate.getTime());

            if (!publicholidays.isEmpty()) {
                emptyWorkingdayFrontend.setPublicholiday(true);
            } else if (nextDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || nextDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                emptyWorkingdayFrontend.setPublicholiday(true);
            }
            emptyWorkingdayFrontendList.add(emptyWorkingdayFrontend);
        }
    }

    private WorkingdayFrontend filterWorkingdayFrontendByDate(Date filterDate, List<WorkingdayFrontend> workingdayFrontendList) {
        List<WorkingdayFrontend> filterWorkingdayFrontend = workingdayFrontendList.stream().
                filter(wd -> wd.getWorkindDayDate().equals(filterDate)).collect(Collectors.toList());
        if (!filterWorkingdayFrontend.isEmpty()) {
            return filterWorkingdayFrontend.get(0);
        }
        return null;
    }

    private List<Workingday> filterByMonth(String month, int year, Employee employee, List<WorkingdayFrontend> emptyWorkingdayFrontendList) throws TimereportException {
        List<Workingday> workingdays = new ArrayList<>();
        try {
            Calendar dateNow = getCalendarNow();
            int monthAsInt = Integer.parseInt(month);

            dateNow.set(Calendar.DAY_OF_MONTH, 1);
            dateNow.set(Calendar.MONTH, monthAsInt);

            dateNow.set(Calendar.YEAR, year);

            Calendar dateMin = (Calendar) dateNow.clone();
            resetDayTime(dateMin);
            dateMin.set(Calendar.DAY_OF_MONTH, 1);
            Date minDate = dateMin.getTime();

            Calendar dateMax = (Calendar) dateMin.clone();
            resetDayTime(dateMax);
            dateMax.set(Calendar.DAY_OF_MONTH, dateNow.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date maxDate = dateMax.getTime();

            workingdays = workingdayService.findByEmployeeAndBetweenDate(employee, minDate, maxDate);

            createEmptyWorkingdayFrontendList(dateMax.getActualMaximum(Calendar.DAY_OF_MONTH), dateMin, emptyWorkingdayFrontendList, employee.getCompany().getFederalstate().getCode());

        } catch (NumberFormatException ex) {
            LOGGER.debug("Bitte Monat als Zahl eingeben.", ex);
            throw new TimereportException("Bitte Monat als Zahl eingeben.");
        }
        return workingdays;
    }

    private List<Workingday> filterByYear(int year, Employee employee, List<WorkingdayFrontend> emptyWorkingdayFrontendList) throws TimereportException {
        List<Workingday> workingdays = new ArrayList<>();

        Calendar dateNow = getCalendarNow();
        dateNow.set(Calendar.DAY_OF_MONTH, 1);

        dateNow.set(Calendar.YEAR, year);

        Calendar dateMin = (Calendar) dateNow.clone();
        resetDayTime(dateMin);
        dateMin.set(Calendar.DAY_OF_YEAR, 1);

        Calendar dateMax = (Calendar) dateMin.clone();
        resetDayTime(dateMax);
        dateMax.set(Calendar.DAY_OF_YEAR, dateNow.getActualMaximum(Calendar.DAY_OF_YEAR));

        workingdays = workingdayService.findByEmployeeAndBetweenDate(employee, dateMin.getTime(), dateMax.getTime());

        createEmptyWorkingdayFrontendList(dateMax.getActualMaximum(Calendar.DAY_OF_YEAR), dateMin, emptyWorkingdayFrontendList, employee.getCompany().getFederalstate().getCode());

        return workingdays;
    }

    @GET
    @Path("/months")
    public String allMonths() {
        List<TimereportMonthsNames> months = Arrays.asList(TimereportMonthsNames.values());
        return GSON_BUILDER.toJson(months);
    }

    private TimereportFrontend parseWorkingdayEntitiesToFrontend(List<Workingday> workingdays, Company employeeCompany, List<WorkingdayFrontend> emptyWorkingdayFrontendList) {
        List<WorkingdayFrontend> workingdayFrontendList = emptyWorkingdayFrontendList;

        TimereportFrontend timereportFrontend = new TimereportFrontend();

        double vacationsDays = 0.0;
        double workingHours = 0.0;
        double minusHours = 0.0;

        double normWorkingHours = 0.0;
        double middaybreakdurationHours = (double) employeeCompany.getMiddaybreakduration() / 60.0;
        double middaybreakFromHours = employeeCompany.getMiddaybreakfrom();

        for (Workingday workingdayEntity : workingdays) {
            WorkingdayFrontend workingdayFrontend = filterWorkingdayFrontendByDate(workingdayEntity.getWorkindDayDate(), workingdayFrontendList);
            workingdayFrontend.setId(workingdayEntity.getId());
            workingdayFrontend.setPublicholiday(workingdayEntity.isPublicholiday());
            workingdayFrontend.setWorkindDayDate(workingdayEntity.getWorkindDayDate());

            Calendar workingdayCalendar = getCalendarNow();
            workingdayCalendar.setTime(workingdayEntity.getWorkindDayDate());

            workingdayFrontend.setCalendarWeek(workingdayCalendar.get(Calendar.WEEK_OF_YEAR));

            workingdayFrontend.setVacationday(workingdayEntity.isVacationday());
            workingdayFrontend.setWorkindDayStart(String.valueOf(workingdayEntity.getWorkindDayStart()));
            workingdayFrontend.setWorkindDayEnd(String.valueOf(workingdayEntity.getWorkindDayEnd()));

            double workingHoursPerDay = 0;
            if (workingdayEntity.isVacationday()) {
                if (!new Double(workingdayEntity.getWorkindDayStart()).equals(0.0)
                        || !new Double(workingdayEntity.getWorkindDayEnd()).equals(0.0)) {
                    vacationsDays = vacationsDays + 0.5;
                    workingHoursPerDay = WORKING_HOURS_PER_DAY_NORM / 2;
                    normWorkingHours = normWorkingHours + WORKING_HOURS_PER_DAY_NORM / 2;

                } else {
                    vacationsDays++;
                }
            } else {

                workingHoursPerDay = workingdayEntity.getWorkindDayEnd() - workingdayEntity.getWorkindDayStart();
                normWorkingHours = normWorkingHours + WORKING_HOURS_PER_DAY_NORM;

            }

            if (workingHoursPerDay >= middaybreakFromHours) {
                workingHoursPerDay = workingHoursPerDay - middaybreakdurationHours;
            }

            workingdayFrontend.setWorkinghours(String.valueOf(workingHoursPerDay));
            workingHours = workingHours + workingHoursPerDay;

        }

        minusHours = normWorkingHours - workingHours;
        if (minusHours > 0) {
            timereportFrontend.setMinusHours(minusHours);
        } else {
            timereportFrontend.setOvertimeHours(workingHours - normWorkingHours);
        }
        timereportFrontend.setVacationdays(vacationsDays);
        timereportFrontend.setWorkingDays(workingdayFrontendList);
        timereportFrontend.setWorkingHours(workingHours);

        return timereportFrontend;
    }

    private Calendar getCalendarNow() {
        Calendar calend = Calendar.getInstance();
        calend.set(Calendar.MILLISECOND, 0);
        calend.set(Calendar.SECOND, 0);
        calend.set(Calendar.MINUTE, 0);
        calend.set(Calendar.HOUR_OF_DAY, 0);
        return calend;
    }

    @DELETE
    @Path("/delete/{workindDayId}")
    public void deleteWorkindDay(
            @PathParam("workindDayId") String workindDayId,
            @Context HttpServletResponse servletResponse,
            @Context SecurityContext sec) throws TimereportException {

        final String loginname = sec.getUserPrincipal().getName();

        Employee employee = employeeService.findByLoginName(loginname);
        if (employee == null) {
            LOGGER.warn("Kein Employee für login " + loginname + " gefunden");
            throw new TimereportException("Bitte melden Sie sich an.");
        }

        Workingday workingday = workingdayService.findByIdAndEmployee(Long.parseLong(workindDayId), employee);

        if (workingday != null) {

            workingdayService.delete(workingday);

        }

        servletResponse.setStatus(HttpServletResponse.SC_OK);

    }
}
