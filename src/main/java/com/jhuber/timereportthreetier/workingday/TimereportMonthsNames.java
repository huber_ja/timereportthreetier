package com.jhuber.timereportthreetier.workingday;

/**
 *
 * @author Janina Huber
 */
public enum TimereportMonthsNames {
    January,February,March,April,May,June,July,August,September,October,November,December
}
