package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Federalstate;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina
 */

@Stateful
public class FederalstateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FederalstateService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct() {
        em.close();
    }

    public Federalstate findById(Long id) {
        return em.find(Federalstate.class, id);
    }

    public List<Federalstate> findAll() {
        TypedQuery<Federalstate> query = em.createQuery(
                "SELECT g FROM Federalstate g ORDER BY g.id", Federalstate.class);
        List<Federalstate> federalstates = query.getResultList();

        return federalstates;
    }

    public Federalstate findByCode(String federalStCode) {
        Federalstate federalstate = null;

        TypedQuery<Federalstate> query = em.createQuery(
                "SELECT f FROM Federalstate f where f.code=:federalStCode", Federalstate.class);
        query.setParameter("federalStCode", federalStCode);

        try {
            federalstate = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double federalstate exists: " + federalstate, nu);
        } catch (NoResultException nr) {
            LOGGER.error("No federalstate found: " + federalstate, nr);
        }

        return federalstate;
    }
}
