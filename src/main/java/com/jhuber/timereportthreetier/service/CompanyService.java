package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Company;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Janina
 */
@Stateful
public class CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct() {
        em.close();
    }

    public void create(Company ne) {
        em.persist(ne);
    }

    public Company findById(Long id) {
        return em.find(Company.class, id);
    }

    public Company findCompanyObj(Company searchCompany) {
        Company company = null;

        TypedQuery<Company> query = em.createQuery(
                "SELECT c FROM Company c where c.address=:companyAddr and c.federalstate.id=:companyFedStID "
                + "and c.name=:compName and c.disabled is null or c.disabled <> 1", Company.class);
        query.setParameter("companyAddr", searchCompany.getAddress());
        query.setParameter("companyFedStCode", searchCompany.getFederalstate().getId());
        query.setParameter("compName", searchCompany.getName());

        try {
            company = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Company exists: " + searchCompany, nu);
        } catch (NoResultException nr) {

        }

        return company;
    }

    public List<Company> findByName(String companyName) {
        TypedQuery<Company> query = em.createQuery(
                "SELECT c FROM Company c where c.name=:companyName and c.disabled is null or c.disabled <> 1", Company.class);
        query.setParameter("companyName", companyName);

        return query.getResultList();
    }

    public void update(Company entity) {
        em.merge(entity);
    }

    public void delete(Company entity) {
        entity.setDisabled(Boolean.TRUE);
        em.merge(entity);
    }

    public List<Company> findAll() {
        TypedQuery<Company> query = em.createQuery(
                "SELECT c FROM Company c where c.disabled is null or c.disabled <> 1 ORDER BY c.id", Company.class);

        return query.getResultList();
    }

}
