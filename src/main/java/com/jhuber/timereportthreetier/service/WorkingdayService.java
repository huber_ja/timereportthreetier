package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Workingday;
import java.util.Date;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Janina
 */
@Stateful
public class WorkingdayService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkingdayService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct(){
        em.close();
    }
    
    public void create(Workingday ne) {
        em.persist(ne);
    }

    public Workingday findById(Long id) {
        return em.find(Workingday.class, id);
    }

     public Workingday findByIdAndEmployee(Long id, Employee employee) {
        Workingday workingday = null;

        TypedQuery<Workingday> query = em.createQuery(
                "SELECT c FROM Workingday c where c.timereport.employee.id=:employeeId "
                + "and c.id=:workingdayId"
                + "", Workingday.class);
        query.setParameter("employeeId", employee.getId());
        query.setParameter("workingdayId", id);

        try {
            workingday = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Workingday exists: " + employee + "; Id: " + id, nu);
        } catch (NoResultException nr) {

        }

        return workingday;
    }
     
    public List<Workingday> findAll() {
        TypedQuery<Workingday> query = em.createQuery(
                "SELECT c FROM Workingday c ORDER BY c.id", Workingday.class);
        return query.getResultList();
    }

    public List<Workingday> findByTimereportId(String timereportId) {

        TypedQuery<Workingday> query = em.createQuery(
                "SELECT c FROM Workingday c where c.timereport.id=:timereportId ORDER BY c.workindDayDate"
                + "", Workingday.class);
        query.setParameter("timereportId", timereportId);

        return query.getResultList();
    }

    public Workingday findByEmployeeAndDate(Employee empl, Date workingdayDate) {

        Workingday workingday = null;

        TypedQuery<Workingday> query = em.createQuery(
                "SELECT c FROM Workingday c where c.timereport.employee.id=:employeeId "
                + "and c.workindDayDate=:workingdayDate"
                + "", Workingday.class);
        query.setParameter("employeeId", empl.getId());
        query.setParameter("workingdayDate", workingdayDate);

        try {
            workingday = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Workingday exists: " + empl.getTimereportuser().getUsername() + "; Date: " + workingdayDate, nu);
//            for(Workingday wd: query.getResultList()){
//                delete(wd);
//            }
        } catch (NoResultException nr) {

        }

        return workingday;
    }
    
    public List<Workingday> findByEmployeeAndBetweenDate(Employee empl, Date minDate, Date maxDate) {
        TypedQuery<Workingday> query = em.createQuery(
                "SELECT c FROM Workingday c where c.timereport.employee.id=:employeeId "
                + "and (c.workindDayDate >= :minDate and c.workindDayDate <= :maxDate) ORDER BY c.workindDayDate"
                + "", Workingday.class);
        query.setParameter("employeeId", empl.getId());
        query.setParameter("minDate", minDate);
        query.setParameter("maxDate", maxDate);


        return query.getResultList();
    }


    public void update(Workingday workingday) {
        em.merge(workingday);
    }
    
    public void delete(Workingday workingday) {
        em.remove(workingday);
        em.flush();
    }

}
