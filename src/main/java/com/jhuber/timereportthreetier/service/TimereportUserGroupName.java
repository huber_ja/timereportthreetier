package com.jhuber.timereportthreetier.service;

/**
 *
 * @author Janina Huber
 */
public enum TimereportUserGroupName {
    admin, user, disabled
}
