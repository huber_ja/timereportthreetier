package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.data.Timereportusergroup;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Janina
 */
@Stateful
public class EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct(){
        em.close();
    }
    
    public void create(Employee ne) {
        em.persist(ne);
    }

    public void createTimereportuser(Timereportuser tu) {
        em.persist(tu);

        Timereportusergroup timereportrole = new Timereportusergroup();
        timereportrole.setUsername(tu.getUsername());
        timereportrole.setGroupname(TimereportUserGroupName.user.name());

        em.persist(timereportrole);
    }

    public Employee findById(Long id) {
        return em.find(Employee.class, id);
    }

    public List<Employee> findAll() {
        TypedQuery<Employee> query = em.createQuery(
                "SELECT c FROM Employee c where c.disabled is null or c.disabled <> 1 ORDER BY c.id", Employee.class);

        return query.getResultList();
    }

    public Employee findExistingEmployee(Employee searchEmployee) {
        Employee employee = null;

        TypedQuery<Employee> query = em.createQuery(
                "SELECT c FROM Employee c where c.company.id=:emplCompanyId and "
                + "c.lastname=:lastname and "
                + "c.firstname=:firstname and "
                + "c.employeenumber=:employeenumber "
                + "and c.disabled is null or c.disabled <> 1", Employee.class);
        query.setParameter("emplCompanyId", searchEmployee.getCompany().getId());
        query.setParameter("lastname", searchEmployee.getLastname());
        query.setParameter("firstname", searchEmployee.getFirstname());
        query.setParameter("employeenumber", searchEmployee.getEmployeenumber());

        try {
            employee = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Employee exists: " + employee, nu);
        } catch (NoResultException nr) {

        }

        return employee;
    }

    public Employee findByEmployeeNumber(String employeeNumber) {

        Employee employee = null;

        TypedQuery<Employee> query = em.createQuery(
                "SELECT c FROM Employee c where c.employeenumber=:employeenumber "
                + "and c.disabled is null or c.disabled <> 1", Employee.class);
        query.setParameter("employeenumber", employeeNumber);

        try {
            employee = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Employee exists: " + employee, nu);
        } catch (NoResultException nr) {

        }

        return employee;
    }
    
    public Employee findByLoginName(String loginname) {

        Employee employee = null;

        TypedQuery<Employee> query = em.createQuery(
                "SELECT c FROM Employee c where c.timereportuser.username=:loginname "
                + "and c.disabled is null or c.disabled <> 1", Employee.class);
        query.setParameter("loginname", loginname);

        try {
            employee = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Employee exists: " + employee, nu);
        } catch (NoResultException nr) {

        }

        return employee;
    }

    public Employee findByEmployeeNumberAndCompany(String employeeNumber, Company employeeCompany) {

        Employee employee = null;

        TypedQuery<Employee> query = em.createQuery(
                "SELECT c FROM Employee c where c.company.id=:companyId "
                + "and c.employeenumber=:employeenumber and c.disabled is null or c.disabled <> 1", Employee.class);
        query.setParameter("employeenumber", employeeNumber);
        query.setParameter("companyId", employeeCompany.getId());

        try {
            employee = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Employee exists: " + employee, nu);
        } catch (NoResultException nr) {

        }

        return employee;
    }

    public void update(Employee employee) {
        em.merge(employee);
    }
    
    public void delete(Employee entity) {
        
       Timereportuser emplUser = entity.getTimereportuser();

       List<Timereportusergroup> timereportUserGroups = findTimereportUSerGroupByLoginname(emplUser.getUsername());
       
       for(Timereportusergroup timereportUserGroup : timereportUserGroups){
           timereportUserGroup.setGroupname(TimereportUserGroupName.disabled.name());
           em.merge(timereportUserGroup);
       }
        
        entity.setDisabled(Boolean.TRUE);
        em.merge(entity);
    }

    
    
    public Timereportuser findTimereportUserByLoginname(String loginname) {
        Timereportuser timereportuser = null;

        TypedQuery<Timereportuser> query = em.createQuery("SELECT c FROM Timereportuser "
                + "c where c.username=:loginname", Timereportuser.class);
        query.setParameter("loginname", loginname);

        try {
            timereportuser = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double Timereportuser exists: " + timereportuser, nu);
        } catch (NoResultException nr) {

        }

        return timereportuser;
    }
    
    public List<Timereportusergroup> findTimereportUSerGroupByLoginname(String loginname) {
        TypedQuery<Timereportusergroup> query = em.createQuery("SELECT c FROM Timereportusergroup c where c.username=:loginname", 
                Timereportusergroup.class);
        query.setParameter("loginname", loginname);

        return query.getResultList();
    }
    
}
