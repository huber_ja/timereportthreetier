package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Timereport;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Janina
 */
@Stateful
public class TimereportService {

    static final Logger LOGGER = LoggerFactory.getLogger(TimereportService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct(){
        em.close();
    }
    
    public void create(Timereport ne) {
        em.persist(ne);
    }
  

    public Timereport findById(Long id) {
        return em.find(Timereport.class, id);
    }

    public List<Timereport> findAll() {
        TypedQuery<Timereport> query = em.createQuery(
                "SELECT c FROM Timereport c ORDER BY c.id", Timereport.class);
        return query.getResultList();
    }

    public List<Timereport> findByEmployeeNumber(String employeeNumber) {

        TypedQuery<Timereport> query = em.createQuery(
                "SELECT c FROM Timereport c where c.employee.employeenumber=:employeenumber "
                + "", Timereport.class);
        query.setParameter("employeenumber", employeeNumber);
   
        return query.getResultList();
    }

}
