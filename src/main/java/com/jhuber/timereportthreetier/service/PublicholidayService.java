package com.jhuber.timereportthreetier.service;

import com.jhuber.timereportthreetier.data.Publicholiday;
import java.util.Date;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Janina
 */
@Stateful
public class PublicholidayService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PublicholidayService.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "timereportThreetierUnit")
    private EntityManager em;

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    @PreDestroy
    public void destruct(){
        em.close();
    }
    
    public void create(Publicholiday ne) {
        em.persist(ne);
    }

    public Publicholiday findById(Long id) {
        return em.find(Publicholiday.class, id);
    }
    
    public Publicholiday findPublicholidayObj(Publicholiday searchPublicholiday) {
        Publicholiday publicholiday = null;

        TypedQuery<Publicholiday> query = em.createQuery(
                "SELECT p FROM Publicholiday p where p.holidayDate=:holiDate and p.holidayDescription=:holiDescr "
                + "and p.federalstate.id=:federalStID", Publicholiday.class);
        query.setParameter("holiDate", searchPublicholiday.getHolidayDate());
        query.setParameter("holiDescr", searchPublicholiday.getHolidayDescription());
        query.setParameter("federalStID", searchPublicholiday.getFederalstate().getId());

        try {
            publicholiday = query.getSingleResult();
        } catch (NonUniqueResultException nu) {
            LOGGER.error("Double holiday exists: " + searchPublicholiday, nu);
        } catch (NoResultException nr) {

        }

        return publicholiday;
    }

     public List<Publicholiday> findByFederalstateCodeAndDate(String federalstateCode, Date holidayAt) {
       
        TypedQuery<Publicholiday> query = em.createQuery(
                "SELECT p FROM Publicholiday p where p.holidayDate=:holiDate and p.federalstate.code=:federalStCode", Publicholiday.class);
        query.setParameter("holiDate", holidayAt);
        query.setParameter("federalStCode", federalstateCode);

        return query.getResultList();
    }
    
    public List<Publicholiday> findByFederalstateCode(String federalstateCode) {

        TypedQuery<Publicholiday> query = em.createQuery(
                "SELECT p FROM Publicholiday p where p.federalstate.code=:federalStCode", Publicholiday.class);
        query.setParameter("federalStCode", federalstateCode);

        return query.getResultList();
    }

    public List<Publicholiday> findByDate(Date holidayAt) {

        TypedQuery<Publicholiday> query = em.createQuery(
                "SELECT p FROM Publicholiday p where p.holidayDate=:holiDate", Publicholiday.class);
        query.setParameter("holiDate", holidayAt);
       
        return query.getResultList();
    }

    public void update(Publicholiday entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void delete(Publicholiday entity) {
        em.remove(entity);
    }

    public List<Publicholiday> findAll() {
        TypedQuery<Publicholiday> query = em.createQuery(
                "SELECT g FROM Publicholiday g ORDER BY g.id", Publicholiday.class);

        return query.getResultList();
    }

}
