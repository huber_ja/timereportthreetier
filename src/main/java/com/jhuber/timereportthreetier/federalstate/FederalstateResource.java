package com.jhuber.timereportthreetier.federalstate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.service.FederalstateService;
import java.util.ArrayList;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Janina
 */
@Stateless
@Path("federalstate")
public class FederalstateResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(FederalstateResource.class);
    
    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); 
   
    @EJB
    public FederalstateService federalstateService;

    
    @GET
    public String allFederalstates() {
        List<Federalstate> federalstates = new ArrayList<>();
        try {

            federalstates = federalstateService.findAll();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        
        return GSON_BUILDER.toJson(federalstates);
    }

}
