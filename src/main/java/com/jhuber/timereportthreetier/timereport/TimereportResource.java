package com.jhuber.timereportthreetier.timereport;

import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Publicholiday;
import com.jhuber.timereportthreetier.data.Timereport;
import com.jhuber.timereportthreetier.data.Workingday;
import com.jhuber.timereportthreetier.service.EmployeeService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import com.jhuber.timereportthreetier.service.TimereportService;
import com.jhuber.timereportthreetier.service.WorkingdayService;
import java.io.BufferedReader;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Janina
 */
@Stateless
@Path("timereport")
public class TimereportResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimereportResource.class);

    private static final String VACATION_DAY_MARKER = "u";

    private static final String TIME_DELIMITER = ":";

    @EJB
    public TimereportService timereportService;

    @EJB
    public EmployeeService employeeService;

    @EJB
    public WorkingdayService workingdayService;

    @EJB
    public PublicholidayService publicholidayService;

    @POST
    @Path("/new")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void addTimereport(
            @FormDataParam("timereportFile") InputStream uploadedInputStream,
            @FormDataParam("timereportFile") FormDataContentDisposition fileDetail,
            @Context HttpServletResponse servletResponse,
            @Context SecurityContext sec) throws TimereportException {

        String loginname = sec.getUserPrincipal().getName();

        Employee employee = employeeService.findByLoginName(loginname);
        if (employee == null) {
            LOGGER.warn("Kein Employee für login " + loginname + " gefunden");
            throw new TimereportException("Bitte melden Sie sich an.");
        }

        if (uploadedInputStream == null || fileDetail == null) {
            throw new TimereportException("Bitte laden Sie eine Textdatei hoch.");
        }

        String federalstatecode = employee.getCompany().getFederalstate().getCode();

        parseAndInsertWorkingdaysFromInputstream(uploadedInputStream, federalstatecode, employee);

        servletResponse.setHeader("Location", "timereport.html");
        servletResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);

    }

    private void parseAndInsertWorkingdaysFromInputstream(InputStream uploadedInputStream, String federalstatecode, Employee employee) throws TimereportException {
        try {

            List<Workingday> workingdays = parseFileToWorkingdayList(uploadedInputStream, federalstatecode);

            if (workingdays.isEmpty()) {
                throw new TimereportException("Es konnten keine Daten eingeselen werden.");
            }

            Timereport tm = createTimereportEntity(employee);

            for (Workingday workingday : workingdays) {
                workingday.setTimereport(tm);

                Workingday updateWorkingDay = workingdayService.findByEmployeeAndDate(employee, workingday.getWorkindDayDate());
                if (updateWorkingDay != null) {
                    updateExistingWorkingday(updateWorkingDay, workingday);
                } else {
                    workingdayService.create(workingday);
                }

            }

        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            java.util.logging.Logger.getLogger(TimereportResource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Timereport createTimereportEntity(Employee employee) {
        Calendar dateNow = getCalendarNow();
        Timereport tm = new Timereport();
        tm.setEmployee(employee);
        tm.setReportMonth(dateNow.get(Calendar.MONTH));
        tm.setReportYear(dateNow.get(Calendar.YEAR));
        timereportService.create(tm);
        return tm;
    }

    private void updateExistingWorkingday(Workingday updateWorkingDay, Workingday workingday) {
        updateWorkingDay.setTimereport(workingday.getTimereport());
        updateWorkingDay.setWorkindDayEnd(workingday.getWorkindDayEnd());
        updateWorkingDay.setWorkindDayStart(workingday.getWorkindDayStart());
        updateWorkingDay.setPublicholiday(workingday.isPublicholiday());
        updateWorkingDay.setVacationday(workingday.isVacationday());
        workingdayService.update(updateWorkingDay);
    }

    private List<Workingday> parseFileToWorkingdayList(InputStream uploadedInputStream, String federalstatecode) throws TimereportException, IOException {
        StringBuilder parseError = new StringBuilder();

        List<Workingday> workingdays = new ArrayList<>();

        List<String> lines = getLinesFromInputStream(uploadedInputStream);

        int lineCount = 1;
        for (String line : lines) {
            String[] lineWorkingday = line.split(";");
            if (lineWorkingday.length != 3) {
                throw new TimereportException("Timereport Zeile " + lineCount + " hat ungültigen Format.");
            }

            Workingday workingday = new Workingday();
            String workingDayDate = lineWorkingday[0];
            String workingDayStart = lineWorkingday[1];
            String workingDayEnd = lineWorkingday[2];

            setPublicholidayProperty(workingDayDate, workingday, federalstatecode, parseError, lineCount);

            setWorkingTimeProp(workingDayStart, workingday, workingDayEnd, parseError, lineCount);

            if (parseError.length() > 0) {
                throw new TimereportException(parseError.toString());
            }

            lineCount++;

            workingdays.add(workingday);
        }
        return workingdays;
    }

    private void setWorkingTimeProp(String workingDayStart, Workingday workingday, String workingDayEnd, StringBuilder parseError, int lineCount) {
        try {
            if (!VACATION_DAY_MARKER.equals(workingDayStart)) {
                workingday.setWorkindDayStart(formatTimeStringToDouble(workingDayStart));
            } else {
                workingday.setVacationday(true);
            }

            if (!VACATION_DAY_MARKER.equals(workingDayEnd)) {
                workingday.setWorkindDayEnd(formatTimeStringToDouble(workingDayEnd));
            } else {
                workingday.setVacationday(true);
            }
        } catch (NumberFormatException ex) {
            parseError.append("Bitte Zeit im Format 08:00 oder 08:45 eingeben. Zeile: ").append(lineCount);
            LOGGER.error(ex.getLocalizedMessage());
        }
    }

    private void setPublicholidayProperty(String workingDayDate, Workingday workingday, String federalstatecode, StringBuilder parseError, int lineCount) {
        try {

            DateFormat FORMATTER = new SimpleDateFormat("dd.MM.yyyy");
            Date workingDayDateAsDate = FORMATTER.parse(workingDayDate);
            workingday.setWorkindDayDate(workingDayDateAsDate);
            List<Publicholiday> publicholidays = publicholidayService.findByFederalstateCodeAndDate(federalstatecode, workingDayDateAsDate);

            if (!publicholidays.isEmpty()) {
                workingday.setPublicholiday(true);
            }
        } catch (ParseException ex) {
            parseError.append("Bitte Datum im Format 01.01.2015 eingeben. Zeile: ")
                    .append(lineCount).append(". Datum: ").append(workingDayDate);
            LOGGER.warn(parseError.toString(), ex);
        } catch (NumberFormatException ex) {
            parseError.append("Bitte Datum im Format 01.01.2015 eingeben. Zeile: ")
                    .append(lineCount).append(". Datum: ").append(workingDayDate);
            LOGGER.warn(parseError.toString(), ex);
        }
    }

    private double formatTimeStringToDouble(String workingDayTime) {

        if (workingDayTime.indexOf(TIME_DELIMITER) < 1) {
            throw new NumberFormatException();
        }

        double hours = Double.parseDouble(workingDayTime.split(TIME_DELIMITER)[0]);
        double minutes = Double.parseDouble(workingDayTime.split(TIME_DELIMITER)[1]);
        double minutesDecimal = minutes / 60;
        return hours + minutesDecimal;
    }

    private Calendar getCalendarNow() {
        Calendar calend = Calendar.getInstance();
        calend.set(Calendar.MILLISECOND, 0);
        calend.set(Calendar.SECOND, 0);
        calend.set(Calendar.MINUTE, 0);
        calend.set(Calendar.HOUR_OF_DAY, 0);
        return calend;
    }

    private List<String> getLinesFromInputStream(InputStream uploadedInputStream) throws IOException {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(uploadedInputStream))) {
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            br.close();
            uploadedInputStream.close();
        }
        return lines;
    }

}
