function showError(jqXHR) {
    $("#error_notification").text(jqXHR.responseText);
}

var ajaxHandleResponse = function (data, textStatus, jqXHR) {
    var moveToLocation = jqXHR.getResponseHeader('Location');
    if (moveToLocation !== undefined && moveToLocation.length > 5) {
        location.href = moveToLocation;
    }
};

function fillFederalstateOptions() {

    $.getJSON("rest/federalstate", function (data) {
        var options = [];
        options.push('<option value="">select federalstate</option>');
        $.each(data, function (key, val) {
            options.push('<option value="' + val.code + '">' + val.name + '</option>');
        });

        $("#federalstate").html(options.join(""));
    });

}

function fillCompanyOptions() {

    $.getJSON("rest/company", function (data) {
        var options = [];
        options.push('<option value="">select company</option>');
        $.each(data, function (key, val) {
            options.push('<option value="' + val.id + '">' + val.name + '</option>');
        });

        $("#company").html(options.join(""));
    });

}
function fillLoginUserName() {
    $.getJSON("rest/auth/empl", function (data) {
        $("#loginUserName").text(data);
    });
}

function fillMonthsOptions() {

    $.getJSON("rest/workingday/months", function (data) {
        var options = [];
        options.push('<option value="">select month</option>');
        var count = 0;
        $.each(data, function (key, val) {
            options.push('<option value="' + count + '">' + val + '</option>');
            count++;
        });

        $("#month").html(options.join(""));
        $('#month option[value="'+(new Date).getMonth()+'"]').attr('selected','selected');
    });
}
