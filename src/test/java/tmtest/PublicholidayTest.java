package tmtest;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Publicholiday;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayCallImportURL;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayMapper;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayResource;
import com.jhuber.timereportthreetier.publicholiday.PublicholidayWebservice;
import com.jhuber.timereportthreetier.service.FederalstateService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Janina
 */
public class PublicholidayTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static PublicholidayResource publicholidayResource;

    private static PublicholidayService publicholidayService;

    private static FederalstateService federalstateService;

    static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").serializeNulls().create();

    private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();
        publicholidayService = new PublicholidayService();
        publicholidayService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        publicholidayResource = new PublicholidayResource();
        publicholidayResource.publicholidayService = publicholidayService;
        publicholidayResource.federalstateService = federalstateService;

        insertTestData();
    }

    @Before
    public void setUp() {
        et.begin();
    }

    @After
    public void tearDown() {
        et.commit();
    }

    public static void insertTestData() throws Exception {

        et.begin();

        List<Federalstate> federalstates = federalstateService.findAll();

        for (Federalstate federalstate : federalstates) {

            String jsonString = PublicholidayCallImportURL.getJsonStringByFederalstate(federalstate.getCode());

            Gson gson = new Gson();
            PublicholidayWebservice[] publicholidayFromWebservices = gson.fromJson(jsonString, PublicholidayWebservice[].class);

            for (PublicholidayWebservice publHoliFromWebservice : publicholidayFromWebservices) {
                Publicholiday importHoliday = PublicholidayMapper.getEntityFromWebserviceObj(publHoliFromWebservice, federalstate);

                if (publicholidayService.findPublicholidayObj(importHoliday) == null) {
                    publicholidayService.create(importHoliday);
                }
            }
        }

        et.commit();

    }

    @Test
    public void findAllPublicholidays() {
        String publicholidaysAsJson = publicholidayResource.allPublicholidays();
        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.size() > 0), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByFederalstate() {

        String federalStateCode = "NW";

        String publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalStateCode(federalStateCode);

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.size() > 0), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByNoExistingFederalstate() {

        String federalStateCode = "Moon";
        String publicholidaysAsJson = "";

        try {
            publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalStateCode(federalStateCode);
        } catch (Exception ex) {
            Logger.getLogger(PublicholidayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Keine Exception wird erwartet!");
        }

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.isEmpty()), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByFederalstateAndDate() {

        String federalStateCode = "NW";
        String holidayAt = "2016-01-01";

        String publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalstateAndDate(federalStateCode, holidayAt);

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.size() > 0), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByNoExistingFederalstateAndDate() {

        String federalStateCode = "Moon";
        String holidayAt = "2015-12-31";
        String publicholidaysAsJson = "";

        try {
            publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalstateAndDate(federalStateCode, holidayAt);
        } catch (Exception ex) {
            Logger.getLogger(PublicholidayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Keine Exception wird erwartet!");
        }

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.isEmpty()), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByFederalstateAndFalseDate() {

        String federalStateCode = "NW";
        String holidayAt = "2015-11-31";
        String publicholidaysAsJson = "";

        try {
            publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalstateAndDate(federalStateCode, holidayAt);
        } catch (Exception ex) {
            Logger.getLogger(PublicholidayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Keine Exception wird erwartet!");
        }

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.isEmpty()), Boolean.TRUE);
    }

    @Test
    public void findPublHolidayByFederalstateAndNoExistingDate() {

        String federalStateCode = "NW";
        String holidayAt = "2015-13-31";
        String publicholidaysAsJson = "";

        try {
            publicholidaysAsJson = publicholidayResource.findPublicholidayByFederalstateAndDate(federalStateCode, holidayAt);
        } catch (Exception ex) {
            Logger.getLogger(PublicholidayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Keine Exception wird erwartet!");
        }

        Type listType = new TypeToken<List<Publicholiday>>() {
        }.getType();

        List<Publicholiday> publicholidaysListFromJson = GSON_BUILDER.fromJson(publicholidaysAsJson, listType);
        assertEquals((publicholidaysListFromJson.isEmpty()), Boolean.TRUE);
    }

    @Test
    public void testPublicholidayMapper() {

        Calendar testDate = Calendar.getInstance();

        testDate.set(Calendar.HOUR_OF_DAY, 0);
        testDate.set(Calendar.MINUTE, 0);
        testDate.set(Calendar.SECOND, 0);
        testDate.set(Calendar.MILLISECOND, 0);

        testDate.set(Calendar.DAY_OF_MONTH, 1);
        testDate.set(Calendar.MONTH, 1);
        testDate.set(Calendar.YEAR, 2016);

        String testDatum = FORMATTER.format(testDate.getTime());
        String testHinweis = "HALLO";
        String testTitle = "NEW YEAR";

        PublicholidayWebservice publHoliFromWebservice = new PublicholidayWebservice();
        publHoliFromWebservice.setDatum(testDatum);
        publHoliFromWebservice.setHinweis(testHinweis);
        publHoliFromWebservice.setTitle(testTitle);

        Federalstate federalstate = new Federalstate();
        federalstate.setCode("NW");

        Publicholiday importHoliday = PublicholidayMapper.getEntityFromWebserviceObj(publHoliFromWebservice, federalstate);

        assertEquals(testDate.getTime(), importHoliday.getHolidayDate());
        assertEquals(testTitle, importHoliday.getHolidayDescription());
        assertEquals(federalstate.getCode(), importHoliday.getFederalstate().getCode());
    }

    @Test
    public void testPublicholidayMapperWhithFalseDateFormat() {

        Calendar testDate = Calendar.getInstance();

        testDate.set(Calendar.HOUR_OF_DAY, 0);
        testDate.set(Calendar.MINUTE, 0);
        testDate.set(Calendar.SECOND, 0);
        testDate.set(Calendar.MILLISECOND, 0);

        testDate.set(Calendar.DAY_OF_MONTH, 1);
        testDate.set(Calendar.MONTH, 0);
        testDate.set(Calendar.YEAR, 2015);

        String testDatum = "01.01.2015";
        String testHinweis = "HALLO";
        String testTitle = "NEW YEAR";

        PublicholidayWebservice publHoliFromWebservice = new PublicholidayWebservice();
        publHoliFromWebservice.setDatum(testDatum);
        publHoliFromWebservice.setHinweis(testHinweis);
        publHoliFromWebservice.setTitle(testTitle);

        Federalstate federalstate = new Federalstate();
        federalstate.setCode("NW");

        Publicholiday importHoliday = new Publicholiday();

        importHoliday = PublicholidayMapper.getEntityFromWebserviceObj(publHoliFromWebservice, federalstate);

        assertEquals(null, importHoliday);

    }

}
