package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Timereport;
import com.jhuber.timereportthreetier.data.TimereportFrontend;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.data.Workingday;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.EmployeeService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import com.jhuber.timereportthreetier.service.TimereportService;
import com.jhuber.timereportthreetier.service.WorkingdayService;
import com.jhuber.timereportthreetier.workingday.TimereportMonthsNames;
import com.jhuber.timereportthreetier.workingday.WorkingdayResource;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Janina
 */
public class WorkingDayTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static CompanyService companyService;

    private static FederalstateService federalstateService;

    private static WorkingdayResource workingdayResource;

    private static TimereportService timereportService;

    private static EmployeeService employeeService;

    private static WorkingdayService workingdayService;

    private static PublicholidayService publicholidayService;

    private static final String companyName = "Test Company Workingday";

    private static final String userName = "testuser";

    private static final String federalStateCode = "NW";

    private static final Gson GSON_BUILDER = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").serializeNulls().create();

    private static final Calendar ACTUAL_DATE = Calendar.getInstance();

    private static final Calendar TEST_MONTH = Calendar.getInstance();

    private static final String employeeNumber = ACTUAL_DATE.getTime().toString();

    public WorkingDayTest() {
    }

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
          
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();

        companyService = new CompanyService();
        companyService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        timereportService = new TimereportService();
        timereportService.setEm(em);

        employeeService = new EmployeeService();
        employeeService.setEm(em);

        workingdayService = new WorkingdayService();
        workingdayService.setEm(em);

        publicholidayService = new PublicholidayService();
        publicholidayService.setEm(em);

        workingdayResource = new WorkingdayResource();
        workingdayResource.timereportService = timereportService;
        workingdayResource.employeeService = employeeService;
        workingdayResource.workingdayService = workingdayService;
        workingdayResource.publicholidayService = publicholidayService;

        insertTestData();
    }

    private static void insertTestData() {
        et.begin();
        Federalstate fedState = federalstateService.findByCode(federalStateCode);

        Company newCompany = new Company();
        newCompany.setName(companyName);
        newCompany.setAddress("Testcity, Mainstreet 1");
        newCompany.setFederalstate(fedState);
        newCompany.setMiddaybreakduration(30);
        newCompany.setMiddaybreakfrom(5);

        companyService.create(newCompany);

        Timereportuser testUser = new Timereportuser();
        testUser.setPassword("1234");
        testUser.setUsername(userName);

        employeeService.createTimereportuser(testUser);

        Employee empl = new Employee();
        empl.setCompany(newCompany);
        empl.setEmployeenumber(employeeNumber);
        empl.setFirstname("Employee");
        empl.setLastname("Test");
        empl.setTimereportuser(testUser);

        employeeService.create(empl);
        {

            TEST_MONTH.set(Calendar.HOUR_OF_DAY, 0);
            TEST_MONTH.set(Calendar.MINUTE, 0);
            TEST_MONTH.set(Calendar.SECOND, 0);
            TEST_MONTH.set(Calendar.MILLISECOND, 0);
            
            TEST_MONTH.set(Calendar.MONTH, 0);
            
            TEST_MONTH.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            TEST_MONTH.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
          
            Timereport timereport = new Timereport();
            timereport.setEmployee(empl);
            timereport.setReportMonth(TEST_MONTH.get(Calendar.MONTH));
            timereport.setReportYear(TEST_MONTH.get(Calendar.YEAR));

            timereportService.create(timereport);

            {
                Workingday workingday = new Workingday();
                workingday.setWorkindDayDate(TEST_MONTH.getTime());
                workingday.setVacationday(true);
                workingday.setTimereport(timereport);
                workingdayService.create(workingday);
            }
            {
                TEST_MONTH.add(Calendar.DAY_OF_YEAR, 1);
                Workingday workingday = new Workingday();
                workingday.setWorkindDayDate(TEST_MONTH.getTime());
                workingday.setVacationday(true);
                workingday.setWorkindDayEnd(18.0);
                workingday.setTimereport(timereport);
                workingdayService.create(workingday);
            }
            {
                TEST_MONTH.add(Calendar.DAY_OF_YEAR, 1);
                Workingday workingday = new Workingday();
                workingday.setWorkindDayDate(TEST_MONTH.getTime());
                workingday.setWorkindDayStart(8.0);
                workingday.setWorkindDayEnd(18.0);
                workingday.setTimereport(timereport);
                workingdayService.create(workingday);
            }
        }
        {
            while (ACTUAL_DATE.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || ACTUAL_DATE.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                ACTUAL_DATE.add(Calendar.DAY_OF_YEAR, 1);
            }

            ACTUAL_DATE.set(Calendar.HOUR_OF_DAY, 0);
            ACTUAL_DATE.set(Calendar.MINUTE, 0);
            ACTUAL_DATE.set(Calendar.SECOND, 0);
            ACTUAL_DATE.set(Calendar.MILLISECOND, 0);
            
            Timereport timereport = new Timereport();
            timereport.setEmployee(empl);
            timereport.setReportMonth(ACTUAL_DATE.get(Calendar.MONTH));
            timereport.setReportYear(ACTUAL_DATE.get(Calendar.YEAR));
  
            timereportService.create(timereport);

            Workingday workingday = new Workingday();
            workingday.setWorkindDayDate(ACTUAL_DATE.getTime());
            workingday.setWorkindDayStart(9.0);
            workingday.setWorkindDayEnd(17.5);
            workingday.setTimereport(timereport);
            workingdayService.create(workingday);

        }
        et.commit();

    }

    @Before
    public void setUp() {
        et.begin();
    }

    @After
    public void tearDown() {
        et.commit();
    }

    @Test
    public void testActualDateWorkingDayList() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        String timereportFrontendAsJson = "";
        try {
            timereportFrontendAsJson = workingdayResource.workingdays(securityContext);
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Gson gson = new Gson();
        TimereportFrontend timereportFrontend = gson.fromJson(timereportFrontendAsJson, TimereportFrontend.class);
        System.out.println("timereportFrontend: "+timereportFrontend.getReportPeriod());
        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getMinusHours()));
        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getOvertimeHours()));
        assertEquals(ACTUAL_DATE.getActualMaximum(Calendar.DAY_OF_MONTH), timereportFrontend.getWorkingDays().size());
        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getVacationdays()));
        assertEquals(Double.valueOf(8.0), Double.valueOf(timereportFrontend.getWorkingHours()));

    }

    @Test
    public void testActualDateWorkingDayListWithNotExistingUser() {

        FakeSecurityContext securityContext = new FakeSecurityContext("noUser");

        try {
            workingdayResource.workingdays(securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte melden Sie sich an.");
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void getMonthsList() {
        String monthsAsJson = workingdayResource.allMonths();

        Type listType = new TypeToken<List<TimereportMonthsNames>>() {
        }.getType();

        List<TimereportMonthsNames> monthsListFromJson = GSON_BUILDER.fromJson(monthsAsJson, listType);
        assertEquals(12, monthsListFromJson.size());
    }

    @Test
    public void testFilterTestMonth() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        String timereportFrontendAsJson = "";
        String testMonth = String.valueOf(TEST_MONTH.get(Calendar.MONTH));
        String testYear = String.valueOf(TEST_MONTH.get(Calendar.YEAR));
        
        try {
            timereportFrontendAsJson = workingdayResource.workingdaysFilter(null, testMonth, testYear, securityContext);
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Gson gson = new Gson();
        TimereportFrontend timereportFrontend = gson.fromJson(timereportFrontendAsJson, TimereportFrontend.class);
        
        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getMinusHours()));
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getOvertimeHours()));
        assertEquals(TEST_MONTH.getActualMaximum(Calendar.DAY_OF_MONTH), timereportFrontend.getWorkingDays().size());
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getVacationdays()));
        assertEquals(Double.valueOf(13.5), Double.valueOf(timereportFrontend.getWorkingHours()));

    }

    @Test
    public void testFilterTestCalendarWeek() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        String timereportFrontendAsJson = "";
        String testCalendarWeek = String.valueOf(TEST_MONTH.get(Calendar.WEEK_OF_YEAR));
       
        String testYear = String.valueOf(TEST_MONTH.get(Calendar.YEAR));

        try {
            timereportFrontendAsJson = workingdayResource.workingdaysFilter(testCalendarWeek, null, testYear, securityContext);
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Gson gson = new Gson();
        TimereportFrontend timereportFrontend = gson.fromJson(timereportFrontendAsJson, TimereportFrontend.class);

        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getMinusHours()));
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getOvertimeHours()));
        assertEquals(TEST_MONTH.getActualMaximum(Calendar.DAY_OF_WEEK), timereportFrontend.getWorkingDays().size());
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getVacationdays()));
        assertEquals(Double.valueOf(13.5), Double.valueOf(timereportFrontend.getWorkingHours()));

    }

    @Test
    public void testFilterTestYear() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        String timereportFrontendAsJson = "";
      
        String testYear = String.valueOf(TEST_MONTH.get(Calendar.YEAR));

        try {
            timereportFrontendAsJson = workingdayResource.workingdaysFilter(null, null, testYear, securityContext);
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Gson gson = new Gson();
        TimereportFrontend timereportFrontend = gson.fromJson(timereportFrontendAsJson, TimereportFrontend.class);
      
        assertEquals(Double.valueOf(0.0), Double.valueOf(timereportFrontend.getMinusHours()));
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getOvertimeHours()));
        assertEquals(TEST_MONTH.getActualMaximum(Calendar.DAY_OF_YEAR), timereportFrontend.getWorkingDays().size());
        assertEquals(Double.valueOf(1.5), Double.valueOf(timereportFrontend.getVacationdays()));
        assertEquals(Double.valueOf(21.5), Double.valueOf(timereportFrontend.getWorkingHours()));

    }
    
    @Test
    public void testFilterTestMonthWithNoExistingUser() {

        FakeSecurityContext securityContext = new FakeSecurityContext("noUser");
        String testMonth = String.valueOf(TEST_MONTH.get(Calendar.MONTH));
        String testYear = String.valueOf(TEST_MONTH.get(Calendar.YEAR));

        try {
            workingdayResource.workingdaysFilter(null, testMonth, testYear, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte melden Sie sich an.");
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }
    
    @Test
    public void testFilterTestMonthWithNoDate() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
      
        try {
            workingdayResource.workingdaysFilter(null, null, null, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals("Bitte Woche, Monat oder Jahr eingeben.", ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }
    
    @Test
    public void testFilterTestMonthWithNoValidWeek() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
       
        try {
            workingdayResource.workingdaysFilter("zwei", null, null, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals("Bitte Kalenderwoche als Zahl eingeben.", ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }
    
    @Test
    public void testFilterTestMonthWithNoValidMonth() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
       
        try {
            workingdayResource.workingdaysFilter(null, "januar", null, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals("Bitte Monat als Zahl eingeben.", ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void testFilterTestMonthWithNoValidYear() {

        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
       
        try {
            workingdayResource.workingdaysFilter(null, null, "zwei", securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals("Bitte Jahr als Zahl eingeben.", ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(WorkingDayTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }
}
