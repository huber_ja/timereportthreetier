package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.login.LoginResource;
import com.jhuber.timereportthreetier.service.EmployeeService;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Janina
 */
public class LoginTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static LoginResource loginResource;

    private static EmployeeService employeeService;

    private static FederalstateService federalstateService;

    private static CompanyService companyService;

    private static final String companyName = "Test Company Logintest";

    private static final String userName = "testuser";

    private static final String federalStateCode = "NW";

    private static final Calendar ACTUAL_DATE = Calendar.getInstance();

    private static final String EMPLOYEE_NUMBER = ACTUAL_DATE.getTime().toString();

    private static final String EMPLOYEE_FIRSTNAME = "TEST";

    private static final String EMPLOYEE_LASTNAME = "LOGIN";

    public LoginTest() {
    }

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();

        employeeService = new EmployeeService();
        employeeService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        companyService = new CompanyService();
        companyService.setEm(em);

        loginResource = new LoginResource();
        loginResource.employeeService = employeeService;

        insertTestData();
    }

    private static void insertTestData() {
        et.begin();
        Federalstate fedState = federalstateService.findByCode(federalStateCode);

        Company newCompany = new Company();
        newCompany.setName(companyName);
        newCompany.setAddress("Testcity, Mainstreet 1");
        newCompany.setFederalstate(fedState);
        newCompany.setMiddaybreakduration(30);
        newCompany.setMiddaybreakfrom(5);

        companyService.create(newCompany);

        Timereportuser testUser = new Timereportuser();
        testUser.setPassword("1234");
        testUser.setUsername(userName);

        employeeService.createTimereportuser(testUser);

        Employee empl = new Employee();
        empl.setCompany(newCompany);
        empl.setEmployeenumber(EMPLOYEE_NUMBER);
        empl.setFirstname(EMPLOYEE_FIRSTNAME);
        empl.setLastname(EMPLOYEE_LASTNAME);
        empl.setTimereportuser(testUser);

        employeeService.create(empl);

        et.commit();
    }

    @Test
    public void testFindEmployee() {
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        HttpServletResponse response = mock(HttpServletResponse.class);
        
        String emplyeeName = loginResource.findEmployee(response, securityContext);

        Gson gson = new Gson();
        String emplyeeNameFromJson = gson.fromJson(emplyeeName, String.class);

        String expectedEmplName = new StringBuilder().append(EMPLOYEE_FIRSTNAME).append(" ").append(EMPLOYEE_LASTNAME).toString();

        assertEquals(expectedEmplName, emplyeeNameFromJson);

    }
    @Test
    public void testLogoutEmployee() {
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        
        
        try {
            loginResource.logout(request, response, securityContext);
        } catch (Exception ex) {
            Logger.getLogger(LoginTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }
    }

    
    @Test
    public void testLogoutWithNoLogin() {
        FakeSecurityContext securityContext = null;
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        
        
        try {
            loginResource.logout(request, response, securityContext);
        } catch (Exception ex) {
            Logger.getLogger(LoginTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void testFindNoExistingEmployee() {
        FakeSecurityContext securityContext = new FakeSecurityContext("noUser");
        HttpServletResponse response = mock(HttpServletResponse.class);

        String emplyeeName = loginResource.findEmployee(response, securityContext);

        Gson gson = new Gson();
        String emplyeeNameFromJson = gson.fromJson(emplyeeName, String.class);

        String expectedEmplName = "";

        assertEquals(expectedEmplName, emplyeeNameFromJson);

    }
    @Test
    public void testFindNoLogin() {
        FakeSecurityContext securityContext = null;
        HttpServletResponse response = mock(HttpServletResponse.class);

        String emplyeeName = loginResource.findEmployee(response, securityContext);

        Gson gson = new Gson();
        String emplyeeNameFromJson = gson.fromJson(emplyeeName, String.class);

        String expectedEmplName = "";

        assertEquals(expectedEmplName, emplyeeNameFromJson);

    }

}
