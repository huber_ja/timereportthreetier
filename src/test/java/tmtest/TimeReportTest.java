package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Timereport;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.EmployeeService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import com.jhuber.timereportthreetier.service.PublicholidayService;
import com.jhuber.timereportthreetier.service.TimereportService;
import com.jhuber.timereportthreetier.service.WorkingdayService;
import com.jhuber.timereportthreetier.timereport.TimereportResource;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Janina
 */
public class TimeReportTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static CompanyService companyService;

    private static FederalstateService federalstateService;

    private static TimereportResource timereportResource;

    private static TimereportService timereportService;

    private static EmployeeService employeeService;

    private static WorkingdayService workingdayService;

    private static PublicholidayService publicholidayService;

    private static final LocalDateTime today = LocalDateTime.now();

    private static final String companyName = "Test Company " + today;

    private static final String employeeNumber = today.toString();

    private static final String userName = "testuser";

    private static final String federalStateCode = "NW";

    public TimeReportTest() {
    }

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();

        companyService = new CompanyService();
        companyService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        timereportService = new TimereportService();
        timereportService.setEm(em);

        employeeService = new EmployeeService();
        employeeService.setEm(em);

        workingdayService = new WorkingdayService();
        workingdayService.setEm(em);

        publicholidayService = new PublicholidayService();
        publicholidayService.setEm(em);

        timereportResource = new TimereportResource();
        timereportResource.timereportService = timereportService;
        timereportResource.employeeService = employeeService;
        timereportResource.workingdayService = workingdayService;
        timereportResource.publicholidayService = publicholidayService;

        insertTestData();
    }

    @Before
    public void setUp() {
        et.begin();
    }

    @After
    public void tearDown() {
        et.commit();
    }

    private static void insertTestData() {
        et.begin();
        Federalstate fedState = federalstateService.findByCode(federalStateCode);

        Company newCompany = new Company();
        newCompany.setName(companyName);
        newCompany.setAddress("Testcity, Mainstreet 1");
        newCompany.setFederalstate(fedState);
        newCompany.setMiddaybreakduration(30);
        newCompany.setMiddaybreakfrom(5);

        companyService.create(newCompany);
        {
            Timereportuser testUser = new Timereportuser();
            testUser.setPassword("1234");
            testUser.setUsername(userName);

            employeeService.createTimereportuser(testUser);

            Employee empl = new Employee();
            empl.setCompany(newCompany);
            empl.setEmployeenumber(employeeNumber);
            empl.setFirstname("Employee");
            empl.setLastname("Test");
            empl.setTimereportuser(testUser);

            employeeService.create(empl);
        }

        et.commit();
    }

    @Test
    public void addTimereport() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        List<Timereport> timereportList = timereportService.findByEmployeeNumber(employeeNumber);

        assertEquals((timereportList.size() > 0), Boolean.TRUE);
    }

    @Test
    public void addTimereportWithNoLogin() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext("noUser");

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte melden Sie sich an.");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void addTimereportWithNoDataFile() {

        InputStream data = null;

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {

            timereportResource.addTimereport(data, fdcd, response, securityContext);

            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte laden Sie eine Textdatei hoch.");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void addTimereportWithEmptyDataFile() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015empty.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
       
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Es konnten keine Daten eingeselen werden.");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void addTimereportWithFalseDateFormat() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015FalseDateFormat.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte Datum im Format 01.01.2015 eingeben. Zeile: 1. Datum: 01-04-2015");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void addTimereportWithFalseFormat() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015FalseFormat.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Timereport Zeile 1 hat ungültigen Format.");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void addTimereportWithFalseTimeFormat() {

        InputStream data = TimeReportTest.class.getResourceAsStream("../testdata/april2015FalseTimeFormat.txt");

        FormDataContentDisposition fdcd = mock(FormDataContentDisposition.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FakeSecurityContext securityContext = new FakeSecurityContext(userName);

        try {
            timereportResource.addTimereport(data, fdcd, response, securityContext);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bitte Zeit im Format 08:00 oder 08:45 eingeben. Zeile: 1");
        } catch (Exception ex) {
            Logger.getLogger(TimeReportTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

    }
}
