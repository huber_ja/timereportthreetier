package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.federalstate.FederalstateResource;
import com.jhuber.timereportthreetier.service.FederalstateService;
import java.lang.reflect.Type;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Janina
 */
public class FederalstateTest {
    
    private static EntityManager em;
   
    private static FederalstateResource federalstateResource;
 
    private static FederalstateService federalstateService;

   
    public FederalstateTest() {
    }
    
    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        
        federalstateService = new FederalstateService();
        federalstateService.setEm(em);
        
        federalstateResource = new FederalstateResource();
        
        federalstateResource.federalstateService = federalstateService;
    }

    
    
    @Test
    public void checkFederalstate() {
        String federalstatesAsJson = federalstateResource.allFederalstates();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Federalstate>>() {
        }.getType();

        List<Federalstate> federalstatesListFromJson = gson.fromJson(federalstatesAsJson, listType);
         
        assertEquals(federalstatesListFromJson.size(), 16);

    }
    
}
