package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.data.Employee;
import com.jhuber.timereportthreetier.data.Federalstate;
import com.jhuber.timereportthreetier.data.Timereportuser;
import com.jhuber.timereportthreetier.data.Timereportusergroup;
import com.jhuber.timereportthreetier.employee.EmployeeResource;
import com.jhuber.timereportthreetier.service.TimereportUserGroupName;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.EmployeeService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.mockito.Mockito.mock;

public class EmployeeTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static EmployeeResource employeeResource;

    private static EmployeeService employeeService;

    private static CompanyService companyService;

    private static FederalstateService federalstateService;

    private static final LocalDateTime today = LocalDateTime.now();

    private static final String companyName = "Test Company " + today;

    private static final String userName = "testuser_" + today;

    public EmployeeTest() {
    }

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();
        employeeService = new EmployeeService();
        employeeService.setEm(em);

        companyService = new CompanyService();
        companyService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        employeeResource = new EmployeeResource();
        employeeResource.companyService = companyService;
        employeeResource.employeeService = employeeService;

        insertTestData();
    }

    public static void insertTestData() throws Exception {
        et.begin();
        Federalstate fedState = federalstateService.findByCode("NW");

        Company newCompany = new Company();
        newCompany.setName(companyName);
        newCompany.setAddress("Testcity, Mainstreet 1");
        newCompany.setFederalstate(fedState);
        newCompany.setMiddaybreakduration(Integer.parseInt("30"));
        newCompany.setMiddaybreakfrom(Integer.parseInt("5"));

        companyService.create(newCompany);
        {
            Timereportuser testUser = new Timereportuser();
            testUser.setPassword("1234");
            testUser.setUsername(userName);

            employeeService.createTimereportuser(testUser);

            Employee empl = new Employee();
            empl.setCompany(newCompany);
            empl.setEmployeenumber(today.toString());
            empl.setFirstname("Employee");
            empl.setLastname("Test");
            empl.setTimereportuser(testUser);

            employeeService.create(empl);
        }

        {
            Timereportuser testUser = new Timereportuser();
            testUser.setPassword("12345");
            testUser.setUsername(userName + "_2");

            employeeService.createTimereportuser(testUser);

            Employee empl = new Employee();
            empl.setCompany(newCompany);
            empl.setEmployeenumber(today.toString() + "_2");
            empl.setFirstname("Employee2");
            empl.setLastname("Test2");
            empl.setTimereportuser(testUser);

            employeeService.create(empl);
        }

        et.commit();

    }

    @Before
    public void setUp() {
        et.begin();
    }

    @After
    public void tearDown() {
        et.commit();
    }

    @Test
    public void getEmployeesList() {
        String employeesAsJson = employeeResource.employees();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Employee>>() {
        }.getType();

        List<String> employeeListFromJson = gson.fromJson(employeesAsJson, listType);
        assertEquals(Boolean.TRUE, employeeListFromJson.size() > 0);

    }

    @Test
    public void createEmployee() {

        Company employeeCompany = companyService.findByName(companyName).get(0);

        String firstname = getRandomString();
        String lastname = getRandomString();
        String employeeNumber = "1_" + today;
        String passwort = "testpassword";
        String passwortrepeat = "testpassword";
        Long companyid = employeeCompany.getId();
        String loginname = getRandomString();

        HttpServletResponse response = mock(HttpServletResponse.class);
       
        try {
            employeeResource.addEmployee(firstname, lastname, employeeNumber, passwort, passwortrepeat, companyid, loginname, response);
        } catch (TimereportException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }
        

        Employee employee = employeeService.findByEmployeeNumber(employeeNumber);

        assertEquals((employee != null), Boolean.TRUE);
    }

    @Test
    public void createEmployeeNotEqualPassword() {

        Company employeeCompany = companyService.findByName(companyName).get(0);

        String firstname = getRandomString();
        String lastname = getRandomString();
        String employeeNumber = "1_" + today;
        String passwort = "testpassword";
        String passwortrepeat = "passwortrepeat";
        Long companyid = employeeCompany.getId();
        String loginname = getRandomString();

        HttpServletResponse response = mock(HttpServletResponse.class);

       
        try {
            employeeResource.addEmployee(firstname, lastname, employeeNumber, passwort, passwortrepeat, companyid, loginname, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Felder passwort und passwortrepeat sind nicht identisch. Geben Sie Ihr Passwort noch mal ein.");
        } catch (Exception ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

        
    }

    private static String getRandomString() {
        byte[] array = new byte[7];
        new Random().nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }

    @Test
    public void createEmployeeWithExistingUser() {

        Company employeeCompany = companyService.findByName(companyName).get(0);

        String firstname = getRandomString();
        String lastname = "Test lastname";
        String employeeNumber = "1_" + today;
        String passwort = "testpassword";
        String passwortrepeat = "passwortrepeat";
        Long companyid = employeeCompany.getId();
        String loginname = userName;

        HttpServletResponse response = mock(HttpServletResponse.class);
       
        try {

            employeeResource.addEmployee(firstname, lastname, employeeNumber, passwort, passwortrepeat, companyid, loginname, response);

            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "User " + userName + " existiert. Bitte anderen loginnamen wählen.");
        } catch (Exception ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

        

    }

    @Test
    public void createEmployeeWithExistingEmloyeeNumber() {

        Company employeeCompany = companyService.findByName(companyName).get(0);

        Employee employee = employeeService.findAll().get(employeeService.findAll().size() - 1);
        String employeeNumber = employee.getEmployeenumber();

        String firstname = getRandomString();
        String lastname = "Test lastname";
        String passwort = "testpassword";
        String passwortrepeat = "passwortrepeat";
        Long companyid = employeeCompany.getId();
        String loginname = getRandomString();

        HttpServletResponse response = mock(HttpServletResponse.class);

       
        try {
            employeeResource.addEmployee(firstname, lastname, employeeNumber, passwort, passwortrepeat, companyid, loginname, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Employeenumber " + employeeNumber + " existiert. Bitte andere Nummer wählen.");
        } catch (Exception ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }
        
    }

    @Test
    public void createEmployeeWithNoExistingCompany() {

        String firstname = getRandomString();
        String lastname = "Test lastname";
        String employeeNumber = "1_" + today;
        String passwort = "testpassword";
        String passwortrepeat = "passwortrepeat";
        Long companyid = getNotExistingCompanyId();
        String loginname = getRandomString();

        HttpServletResponse response = mock(HttpServletResponse.class);

       

        try {
            employeeResource.addEmployee(firstname, lastname, employeeNumber, passwort, passwortrepeat, companyid, loginname, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Feld Company ungültig.");
        } catch (Exception ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }
        

    }

    private Long getNotExistingCompanyId() {
        Long companyId = new Random().nextLong();
        Company companyUpdated = companyService.findById(companyId);
        while (companyUpdated != null) {
            companyId = new Random().nextLong();
            companyUpdated = companyService.findById(companyId);
        }
        return companyId;
    }

    @Test
    public void deleteEmployee() {

        Employee employee = employeeService.findAll().get(employeeService.findAll().size() - 1);
        String employeeNumber = employee.getEmployeenumber();
        HttpServletResponse response = mock(HttpServletResponse.class);

       

        employeeResource.deleteEmployee(employeeNumber, response);

        

        Employee employeeDisabled = employeeService.findById(employee.getId());

        List<Timereportusergroup> timereportUserGroups = employeeService.findTimereportUSerGroupByLoginname(employeeDisabled.getTimereportuser().getUsername());

        timereportUserGroups.stream().forEach((timereportUserGroup) -> {
            assertEquals(timereportUserGroup.getGroupname().equals(TimereportUserGroupName.disabled.name()), Boolean.TRUE);
        });
        
        assertEquals(employeeDisabled.getDisabled(), Boolean.TRUE);

    }
}
