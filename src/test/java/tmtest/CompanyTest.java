package tmtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.Gson;
import com.jhuber.timereportthreetier.company.CompanyResource;
import com.jhuber.timereportthreetier.data.Company;
import com.jhuber.timereportthreetier.service.CompanyService;
import com.jhuber.timereportthreetier.service.FederalstateService;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.google.gson.reflect.TypeToken;
import com.jhuber.timereportthreetier.TimereportException;
import com.jhuber.timereportthreetier.data.Federalstate;
import java.lang.reflect.Type;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Janina
 */
public class CompanyTest {

    private static EntityManager em;

    private static EntityTransaction et;

    private static CompanyResource companyResource;

    private static CompanyService companyService;

    private static FederalstateService federalstateService;

    private static final LocalDateTime today = LocalDateTime.now();

    public CompanyTest() {
    }

    @BeforeClass
    public static void preparePersistenceTest() throws Exception {
        em = Persistence.createEntityManagerFactory("timereportThreetierTestUnit").createEntityManager();
        et = em.getTransaction();

        companyService = new CompanyService();
        companyService.setEm(em);

        federalstateService = new FederalstateService();
        federalstateService.setEm(em);

        companyResource = new CompanyResource();
        companyResource.companyService = companyService;
        companyResource.federalstateService = federalstateService;
        insertTestData();
    }
    
    @Before
    public void setUp() {
        et.begin();
    }

    @After
    public void tearDown() {
        et.commit();
    }


    private static void insertTestData() {
        et.begin();
        Federalstate fedState = federalstateService.findByCode("NW");

        Company newCompany = new Company();

        newCompany.setAddress("Javacity, Main street 1");
        newCompany.setFederalstate(fedState);
        newCompany.setMiddaybreakduration(30);
        newCompany.setMiddaybreakfrom(4);
        newCompany.setName("TEST DATA " + today);

        companyService.create(newCompany);

        et.commit();
    }

    @Test
    public void getCompaniesList() {
        String companieAsJson = companyResource.allCompanies();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Company>>() {
        }.getType();

        List<String> companieListFromJson = gson.fromJson(companieAsJson, listType);
        assertEquals((companieListFromJson.size() > 0), Boolean.TRUE);

    }

    @Test
    public void createCompany() {

        String name = "NEW Java Company" + today;
        String address = "Javacity, Main street 1";
        String federalStateCode = "NW";
        int middaybreakduration = 30;
        int middaybreakfrom = 4;
        HttpServletResponse response = mock(HttpServletResponse.class);

       
        try {
            companyResource.addCompany(name, address, federalStateCode, middaybreakfrom, middaybreakduration, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }
        

        List<Company> companies = companyService.findByName(name);

        assertEquals((companies.size() > 0), Boolean.TRUE);
    }

    @Test
    public void createCompanyWithNotExistingFederalstate() {

        String name = "NotExistingFederalstate Company" + today;
        String address = "Javacity, Main street 1";
        String federalStateCode = "NOFEDState";
        int middaybreakduration = 30;
        int middaybreakfrom = 4;
        HttpServletResponse response = mock(HttpServletResponse.class);

       
        try {
            companyResource.addCompany(name, address, federalStateCode, middaybreakfrom, middaybreakduration, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(ex.getMessage(), "Bundesland ist ungültig");
        } catch (Exception ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("TimereportException Exception wird erwartet!");
        }

        
    }

    @Test
    public void updateCompanyName() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "TEST2NAME" + today;

        try {
            companyResource.updateCompany(companyId, "companyname", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        

        Company companyUpdated = companyService.findById(companyId);

        assertEquals(companyUpdated.getName(), editCompanyProp);
    }

    @Test
    public void updateCompanymiddaybreakduration() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "13";

        try {
            companyResource.updateCompany(companyId, "middaybreakduration", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Company companyUpdated = companyService.findById(companyId);

        assertEquals(companyUpdated.getMiddaybreakduration(), Integer.parseInt(editCompanyProp));
    }

    @Test
    public void updateCompanyNotNumericMiddaybreakduration() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "drei";

        try {
            companyResource.updateCompany(companyId, "middaybreakduration", editCompanyProp, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNotNull(ex.getMessage());
        } catch (Exception ex) {
            fail("TimereportException Exception wird erwartet!");
        }

        

    }

    @Test
    public void updateCompanyMiddaybreakfrom() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "10";

        try {
            companyResource.updateCompany(companyId, "middaybreakfrom", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        Company companyUpdated = companyService.findById(companyId);

        assertEquals(companyUpdated.getMiddaybreakfrom(), Integer.parseInt(editCompanyProp));
    }

    @Test
    public void updateCompanyNotNumericMiddaybreakfrom() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "acht";

        try {
            companyResource.updateCompany(companyId, "middaybreakfrom", editCompanyProp, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNotNull(ex.getMessage());
        } catch (Exception ex) {
            fail("TimereportException Exception wird erwartet!");
        }

    }

    @Test
    public void updateCompanyFederalstate() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "BW";
     
        try {
            companyResource.updateCompany(companyId, "federalstate", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }
     
        Company companyUpdated = companyService.findById(companyId);

        assertEquals(companyUpdated.getFederalstate().getCode(), editCompanyProp);
    }

    @Test
    public void updateCompanyNotExistingFederalstate() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();

        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "HAHAHA";
    
        try {
            companyResource.updateCompany(companyId, "federalstate", editCompanyProp, response);
            fail("keine Exception geworfen!");
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNotNull(ex.getMessage());
        } catch (Exception ex) {
            fail("TimereportException Exception wird erwartet!");
        }
        
    }

    @Test
    public void updateNotExistingCompany() {
        Long companyId = getNotExistingCompanyId();
        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "EDIT";

       
        try {
            companyResource.updateCompany(companyId, "address", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("keine Exception erwartet!");
        } catch (Exception ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("keine Exception wird erwartet!");
        }
        
    }

    private Long getNotExistingCompanyId() {
        Long companyId = new Random().nextLong();
        Company companyUpdated = companyService.findById(companyId);
        while (companyUpdated != null) {
            companyId = new Random().nextLong();
            companyUpdated = companyService.findById(companyId);
        }
        return companyId;
    }

    @Test
    public void updateCompanyAddress() {
        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();
        HttpServletResponse response = mock(HttpServletResponse.class);

        String editCompanyProp = "TEST2Adress" + today;

       

        try {
            companyResource.updateCompany(companyId, "address", editCompanyProp, response);
        } catch (TimereportException ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail(ex.getMessage());
        }

        

        Company companyUpdated = companyService.findById(companyId);

        assertEquals(companyUpdated.getAddress(), editCompanyProp);
    }

    @Test
    public void deleteCompany() {

        Company company = companyService.findAll().get(companyService.findAll().size() - 1);
        Long companyId = company.getId();
        HttpServletResponse response = mock(HttpServletResponse.class);
      
       

        companyResource.deleteCompany(companyId, response);

        

        Company companyDisabled = companyService.findById(companyId);

        assertEquals(companyDisabled.getDisabled(), Boolean.TRUE);

    }

    public void deleteNotExistingCompany() {

        Long companyId = getNotExistingCompanyId();
        HttpServletResponse response = mock(HttpServletResponse.class);
        
       
        try {
            companyResource.deleteCompany(companyId, response);
        } catch (Exception ex) {
            Logger.getLogger(CompanyTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Keine Exception wird erwartet!");
        }
        
    }

}
