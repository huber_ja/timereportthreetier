package tmtest;


import java.security.Principal;
import javax.ws.rs.core.SecurityContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Janina Huber
 */
public class FakeSecurityContext implements SecurityContext{

    private static String userName;
    
    public FakeSecurityContext(String username) {
        userName = username;
    }

    @Override
    public Principal getUserPrincipal() {
        Principal testAdminUser = new Principal() {

            @Override
            public String getName() {
                return userName;
            }
        };
        
        return testAdminUser;
    }

    @Override
    public boolean isUserInRole(String string) {
        return true;
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public String getAuthenticationScheme() {
        return "admin";
    }
    
}
